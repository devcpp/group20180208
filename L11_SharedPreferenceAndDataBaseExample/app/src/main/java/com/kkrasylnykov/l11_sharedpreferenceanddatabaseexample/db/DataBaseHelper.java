package com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.ToolsAndConstants.DbConstants;

public class DataBaseHelper extends SQLiteOpenHelper {
    public DataBaseHelper(Context context) {
        super(context, DbConstants.DB_NAME, null, DbConstants.DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + DbConstants.TABLE_NAME + " ("+
                DbConstants.FIELD_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                DbConstants.FIELD_NAME + " TEXT NOT NULL, " +
                DbConstants.FIELD_SNAME + " TEXT, "+
                DbConstants.FIELD_PHONE + " TEXT, " +
                DbConstants.FIELD_ADRESS + " TEXT);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
