package com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.activities;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.R;
import com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.ToolsAndConstants.AppSettings;
import com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.ToolsAndConstants.DbConstants;
import com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.db.DataBaseHelper;

import java.util.ArrayList;
import java.util.HashSet;

public class MainActivity extends AppCompatActivity {

    private static final int REQUEST_CODE_TC = 1001;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        HashSet<Float> arr = new HashSet<>();

        AppSettings appSettings = new AppSettings(this);
        if (appSettings.isNeedShowTC()) {
            Intent intent = new Intent(this, TandCActivity.class);
            startActivityForResult(intent, REQUEST_CODE_TC);

            DataBaseHelper dataBaseHelper = new DataBaseHelper(this);
            SQLiteDatabase db = dataBaseHelper.getWritableDatabase();

            for (int i=0; i<10; i++) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(DbConstants.FIELD_NAME, "Name" + i);
                contentValues.put(DbConstants.FIELD_SNAME, "SName" + i);
                contentValues.put(DbConstants.FIELD_PHONE, "09" + i + "052465" + i*10);
                contentValues.put(DbConstants.FIELD_ADRESS, "str. Devichya " + i);
                long id = db.insert(DbConstants.TABLE_NAME, null, contentValues);

                Log.d("devcpp", "insert -> id -> " + id);
            }
            db.close();
        }

        DataBaseHelper dataBaseHelper = new DataBaseHelper(this);
        SQLiteDatabase db = dataBaseHelper.getReadableDatabase();

        Cursor cursor = db.query(DbConstants.TABLE_NAME, null, null, null,
                null, null, null);

        if (cursor != null) {
            int idPosition = cursor.getColumnIndex(DbConstants.FIELD_ID);
            int namePosition = cursor.getColumnIndex(DbConstants.FIELD_NAME);
            int snamePosition = cursor.getColumnIndex(DbConstants.FIELD_SNAME);
            int phonePosition = cursor.getColumnIndex(DbConstants.FIELD_PHONE);
            int adressPosition = cursor.getColumnIndex(DbConstants.FIELD_ADRESS);
            if (cursor.moveToFirst()) {
                do{
                    long id = cursor.getLong(idPosition);
                    String name = cursor.getString(namePosition);
                    String sname = cursor.getString(snamePosition);
                    String phone = cursor.getString(phonePosition);
                    String adress = cursor.getString(adressPosition);

                    Log.d("devcpp","id -> " + id + "; name -> " + name
                            + "; sname -> " + sname + "; phone -> " + phone
                            + "; adress -> " + adress);

                } while(cursor.moveToNext());
            }
            cursor.close();
        }
        db.close();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_TC) {
            AppSettings appSettings = new AppSettings(this);
            if (appSettings.isNeedShowTC()) {
                Toast.makeText(this, "Вы не приняли соглашение!(((", Toast.LENGTH_LONG).show();
                finish();
            }
        }
    }
}
