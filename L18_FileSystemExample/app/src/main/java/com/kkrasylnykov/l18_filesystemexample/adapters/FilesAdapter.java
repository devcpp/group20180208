package com.kkrasylnykov.l18_filesystemexample.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kkrasylnykov.l18_filesystemexample.R;

import java.io.File;
import java.util.ArrayList;

public class FilesAdapter extends RecyclerView.Adapter<FilesAdapter.FileVH> {

    private ArrayList<File> data;
    private OnFileSelectListener listener;


    public FilesAdapter(ArrayList<File> data) {
        this.data = data;
    }

    @NonNull
    @Override
    public FileVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new FileVH(
                LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_file, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull FileVH holder, int position) {
        holder.name.setText(data.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setListener(OnFileSelectListener listener) {
        this.listener = listener;
    }

    public class FileVH extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView name;

        public FileVH(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.textFileItem);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (listener!=null) {
                listener.onFileSelect(data.get(getAdapterPosition()));
            }
        }
    }

    public interface OnFileSelectListener{
        void onFileSelect(File file);
    }
}
