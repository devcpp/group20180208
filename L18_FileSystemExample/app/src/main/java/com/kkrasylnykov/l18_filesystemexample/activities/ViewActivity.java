package com.kkrasylnykov.l18_filesystemexample.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import com.kkrasylnykov.l18_filesystemexample.R;

import java.io.File;

public class ViewActivity extends AppCompatActivity {

    private static final String KEY_FILE_PATH = "KEY_FILE_PATH";

    private ImageView imageView;

    public static Intent getNewIntent(Context context, String filePath){
        Intent intent = new Intent(context, ViewActivity.class);
        intent.putExtra(KEY_FILE_PATH, filePath);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view);

        imageView = findViewById(R.id.imgView);
        String path = "";
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            path = bundle.getString(KEY_FILE_PATH, "");
        }

        if (!path.isEmpty()) {
            File imgFile = new  File(path);

            if(imgFile.exists()){
                Bitmap bitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                imageView.setImageBitmap(bitmap);

            }
        } else {

        }
    }
}
