package com.kkrasylnykov.l18_filesystemexample.activities;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.kkrasylnykov.l18_filesystemexample.R;
import com.kkrasylnykov.l18_filesystemexample.adapters.FilesAdapter;

import java.io.File;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private static final int REQUEST_PERMISSION_ON_READ_FILE = 1001;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.recyclerView);
//        Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(MainActivity.this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    REQUEST_PERMISSION_ON_READ_FILE);
        } else {
            loadAndShowData();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_PERMISSION_ON_READ_FILE) {
            if(ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "NEED PERMISION", Toast.LENGTH_LONG).show();
                finish();
            } else {
                loadAndShowData();
            }
        }
    }

    private void loadAndShowData(){
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        FilesAdapter adapter = new FilesAdapter(getAllFiles(Environment.getExternalStorageDirectory()));
        adapter.setListener(new FilesAdapter.OnFileSelectListener() {
            @Override
            public void onFileSelect(File file) {
                startActivity(ViewActivity.getNewIntent(MainActivity.this,
                        file.getAbsolutePath()));
            }
        });
        recyclerView.setAdapter(adapter);
    }

    private ArrayList<File> getAllFiles(File file){
        Log.d("devcpp", ">> " + file.getAbsolutePath());
        ArrayList<File> result = new ArrayList<>();
        if (file.isDirectory()) {
            File[] files = file.listFiles();
            if (files != null) {
                for (File curFile : files) {
                    result.addAll(getAllFiles(curFile));
                }
            }
        } else if (file.isFile()) {
            if (file.getName().contains(".png") || file.getName().contains(".jpg")) {
                Log.d("devcpp", ">> " + file.getName());
                result.add(file);
            }
        }
        return result;
    }
}
