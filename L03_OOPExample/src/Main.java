import java.util.Scanner;

import data.Square;

public class Main {
	
	public static void changeSquare (Square square) {
		square.setA(90);
		
		square = new Square(100);
		System.out.println("changeSquare = " + square);
	}

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		Square square = new Square(45);
		Square square2 = square;
		
		changeSquare(square2);
		
		System.out.println("square1 = " + square);
		
		System.out.println("a = " + square.getA());
		System.out.println("S = " + square.getS());
		System.out.println("P = " + square.getP());
		System.out.println("D = " + square.getDiagonal());
		
		square = null;
		System.out.println("square2 = " + square);
		
		square = new Square();
		System.out.println("square3 = " + square);
		System.out.println("square4 = " + square2);
		
		System.out.println("square4.getA() = " + square.getA());
	}

}
