package data;

public class Square {
	
	private float a;
	
	public Square() {
		this(10);
	}
	
	public Square(float a) {
		this(a, 1);
	}
	
	public Square(float a, int b) {
		this.a = a*b;
	}
	
	public void setS(float s) {
		a = (float) Math.sqrt(s);
	}
	
	public float getS() {
		return a*a;
	}
	
	public void setP(float p) {
		a = p/4;
	}
	
	public float getP() {
		return a*4;
	}
	
	public void setDiagonal(float d) {
		a = (float)(d/Math.sqrt(2));
	}
	
	public float getDiagonal() {
		return (float)(a*Math.sqrt(2));
	}
	
	public void setA(float a) {
		this.a = a;
	}
	
	public float getA() {
		return a;
	}
	
}
