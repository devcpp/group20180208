package com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.engines;

import android.content.Context;

import com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.data.Contact;
import com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.providers.db.ContactDBProvider;

import java.util.ArrayList;

public class ContactEngine extends BaseEngine {

    public ContactEngine(Context context) {
        super(context);
    }

    public ArrayList<Contact> getAll(){
        ContactDBProvider dbProvider = new ContactDBProvider(getContext());
        ArrayList<Contact> data = dbProvider.getAll();
        if (data != null) {
            PhoneEngine phoneEngine = new PhoneEngine(getContext());
            for (Contact contact : data){
                contact.setPhones(phoneEngine.getPhonesByUserId(contact.getId()));
            }
        }
        return data;
    }

    public Contact getItemById(long id){
        ContactDBProvider dbProvider = new ContactDBProvider(getContext());
        return dbProvider.getItemById(id);
    }

    public void insetrItem(Contact contact){
        ContactDBProvider dbProvider = new ContactDBProvider(getContext());
        dbProvider.insetrItem(contact);
    }

    public void insetrItems(ArrayList<Contact> contacts){
        ContactDBProvider dbProvider = new ContactDBProvider(getContext());
        dbProvider.insetrItems(contacts);
    }

    public void updateItem(Contact contact){
        ContactDBProvider dbProvider = new ContactDBProvider(getContext());
        dbProvider.updateItem(contact);
    }

    public void removeItem(Contact contact){
        removeItemById(contact.getId());
    }

    public void removeItemById(long id){
        ContactDBProvider dbProvider = new ContactDBProvider(getContext());
        dbProvider.removeItemById(id);
    }

    public void removeAll(){
        ContactDBProvider dbProvider = new ContactDBProvider(getContext());
        dbProvider.removeAll();
    }
}
