package com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.engines;

import android.content.Context;

import com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.data.Phone;
import com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.providers.db.PhoneDBProvider;

import java.util.ArrayList;

public class PhoneEngine extends BaseEngine {
    public PhoneEngine(Context context) {
        super(context);
    }

    public ArrayList<Phone> getPhonesByUserId(long userId) {
        PhoneDBProvider phoneDBProvider = new PhoneDBProvider(getContext());
        return phoneDBProvider.getPhonesByUserId(userId);
    }
}
