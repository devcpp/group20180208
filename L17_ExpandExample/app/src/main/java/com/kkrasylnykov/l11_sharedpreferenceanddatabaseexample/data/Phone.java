package com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.data;

public class Phone extends Entity {
    private long id;
    private long userId;
    private String phone;

    public Phone(String phone) {
        this(-1, -1, phone);
    }

    public Phone(long userId, String phone) {
        this(-1, userId, phone);
    }

    public Phone(long id, long userId, String phone) {
        this.id = id;
        this.userId = userId;
        this.phone = phone;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public boolean equals(Object obj) {
        boolean result = false;
        if (obj instanceof Phone) {
            Phone phoneObj = (Phone) obj;
            result = this.id == phoneObj.id;
        }
        return result;
    }
}
