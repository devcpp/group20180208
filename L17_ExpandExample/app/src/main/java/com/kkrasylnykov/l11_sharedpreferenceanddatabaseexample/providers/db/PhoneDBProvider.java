package com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.providers.db;

import android.content.Context;
import android.database.Cursor;

import com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.ToolsAndConstants.DbConstants;
import com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.data.Phone;

import java.util.ArrayList;

public class PhoneDBProvider extends BaseDBProvider {
    public PhoneDBProvider(Context context) {
        super(context, DbConstants.DB_V2.TABLE_PHONES.TABLE_NAME);
    }

    public ArrayList<Phone> getPhonesByUserId(long userId) {
        ArrayList<Phone> result = new ArrayList<>();
        String selection = DbConstants.DB_V2.TABLE_PHONES.FIELDS.USER_ID + "=?";
        String[] args = new String[]{Long.toString(userId)};
        Cursor cursor = getReadableDatabase().query(getTableName(), null, selection, args,
                null, null, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                int idPosition = cursor.getColumnIndex(DbConstants.DB_V2.TABLE_PHONES.FIELDS.ID);
                int userIdPosition = cursor.getColumnIndex(DbConstants.DB_V2.TABLE_PHONES.FIELDS.USER_ID);
                int phonePosition = cursor.getColumnIndex(DbConstants.DB_V2.TABLE_PHONES.FIELDS.PHONE);
                do {
                    long id = cursor.getLong(idPosition);
                    long idUser = cursor.getLong(userIdPosition);
                    String phone = cursor.getString(phonePosition);

                    result.add(new Phone(id, idUser, phone));
                } while (cursor.moveToNext());
            }
            cursor.close();
        }
        return result;
    }
}
