package com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.R;
import com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.ToolsAndConstants.AppSettings;

public class TandCActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_t_and_c);

        findViewById(R.id.btnOkTandCActivity).setOnClickListener(this);
        findViewById(R.id.btnNoTandCActivity).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btnOkTandCActivity) {
            AppSettings appSettings = new AppSettings(this);
            appSettings.setIsNeedShowTC(false);
        }
        finish();
    }
}
