package com.kkrasylnykov.l07_activityandlogexample.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.kkrasylnykov.l07_activityandlogexample.R;

public class SecondActivity extends AppCompatActivity
        implements View.OnClickListener {
    public static final String KEY_STR_NAME = "KEY_STR_NAME";

    public static final String KEY_RETURN_DATA = "KEY_RETURN_DATA";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        String data = "";
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            data = bundle.getString(KEY_STR_NAME, "");
        }

        Log.d("devcpp", "SecondActivity -> onCreate -> data ->" + data);

        if (data.isEmpty()) {
            Toast.makeText(this, "WRONG DATA!!!!",
                    Toast.LENGTH_LONG).show();
            finish();
            return;
        }

        TextView nameTextView = findViewById(R.id.nameTextViewSecondAcivity);
        nameTextView.setText("Hello, " + data + "!!!!");

        Button noButton = findViewById(R.id.noButton);
        Button yes1Button = findViewById(R.id.yes1Button);
        Button yes2Button = findViewById(R.id.yes2Button);

        noButton.setOnClickListener(this);
        yes1Button.setOnClickListener(this);
        yes2Button.setOnClickListener(this);

        Log.d("devcpp", "SecondActivity -> onCreate -> ");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.noButton:
                setResult(RESULT_CANCELED);
                break;
            case R.id.yes1Button:
            case R.id.yes2Button:
                Intent yesIntent = new Intent();
                yesIntent.putExtra(KEY_RETURN_DATA, v.getId());
                setResult(RESULT_OK, yesIntent);
                break;
        }
        finish();
    }

    @Override
    public void onBackPressed() {
        Toast.makeText(this, "Need click on button!!!!",
                Toast.LENGTH_LONG).show();
//        super.onBackPressed();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("devcpp", "SecondActivity -> onStart -> ");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("devcpp", "SecondActivity -> onResume -> ");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d("devcpp", "SecondActivity -> onRestart -> ");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("devcpp", "SecondActivity -> onPause -> ");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("devcpp", "SecondActivity -> onStop -> ");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("devcpp", "SecondActivity -> onDestroy -> ");
    }
}
