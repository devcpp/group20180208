package com.kkrasylnykov.l07_activityandlogexample;

import android.app.Application;
import android.util.Log;

public class ExampleApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d("devcpp", "Application -> onCreate -> ");
    }
}
