package com.kkrasylnykov.l07_activityandlogexample.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.kkrasylnykov.l07_activityandlogexample.R;

public class MainActivity extends AppCompatActivity
        implements View.OnClickListener {

    private static final int REQUEST_CODE_SECOND_ACTIVITY = 1001;

    private EditText nameEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button button = findViewById(R.id.toSecondActivity);
        button.setOnClickListener(this);

        nameEditText = findViewById(R.id.nameEditTextMainActivity);

        Log.d("devcpp", "MainActivity -> onCreate -> ");
    }

    @Override
    public void onClick(View v) {
        Log.d("devcpp", "MainActivity -> onClick -> ");
        String strData = nameEditText.getText().toString();
        Log.d("devcpp", "MainActivity -> onClick -> strData -> " + strData);
        if (strData.isEmpty()) {
            Toast.makeText(this, "Need entering name!!!!", Toast.LENGTH_LONG).show();
            return;
        }
        Intent intent = new Intent(this, SecondActivity.class);
        intent.putExtra(SecondActivity.KEY_STR_NAME, strData);
        startActivityForResult(intent, REQUEST_CODE_SECOND_ACTIVITY);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_SECOND_ACTIVITY) {
            Log.d("devcpp", "MainActivity -> onActivityResult -> SECOND_ACTIVITY");
            if (resultCode == RESULT_CANCELED) {
                Log.d("devcpp", "MainActivity -> onActivityResult -> SECOND_ACTIVITY -> RESULT_CANCELED");
            } else if (resultCode == RESULT_OK) {
                Log.d("devcpp", "MainActivity -> onActivityResult -> SECOND_ACTIVITY -> RESULT_OK");
                int buttonId = -1;
                if (data != null) {
                    Bundle bundle = data.getExtras();
                    buttonId = bundle.getInt(SecondActivity.KEY_RETURN_DATA,
                            -1);
                }

                Log.d("devcpp", "MainActivity -> onActivityResult -> SECOND_ACTIVITY -> RESULT_OK -> " + buttonId);

                switch (buttonId) {
                    case -1:
                        Toast.makeText(this, "NO DATA!!!!",
                                Toast.LENGTH_LONG).show();
                        break;
                    case R.id.yes1Button:
                        Toast.makeText(this, "yes1!!!!",
                                Toast.LENGTH_LONG).show();
                        break;
                    case R.id.yes2Button:
                        Toast.makeText(this, "yes2!!!!",
                                Toast.LENGTH_LONG).show();
                        break;
                }

                nameEditText.setText("");
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("devcpp", "MainActivity -> onStart -> ");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("devcpp", "MainActivity -> onResume -> ");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d("devcpp", "MainActivity -> onRestart -> ");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("devcpp", "MainActivity -> onPause -> ");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("devcpp", "MainActivity -> onStop -> ");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("devcpp", "MainActivity -> onDestroy -> ");
    }
}
