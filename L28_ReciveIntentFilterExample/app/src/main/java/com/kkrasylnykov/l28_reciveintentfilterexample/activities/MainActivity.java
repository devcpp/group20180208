package com.kkrasylnykov.l28_reciveintentfilterexample.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.kkrasylnykov.l28_reciveintentfilterexample.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
