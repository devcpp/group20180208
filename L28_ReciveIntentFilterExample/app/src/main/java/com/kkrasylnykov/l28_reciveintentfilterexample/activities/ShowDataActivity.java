package com.kkrasylnykov.l28_reciveintentfilterexample.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.kkrasylnykov.l28_reciveintentfilterexample.R;

public class ShowDataActivity extends AppCompatActivity {

    private TextView textView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_data);

        String text = "";

        Intent intent = getIntent();
        if (intent.getAction() != null
                && intent.getAction().equals(Intent.ACTION_SEND)) {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                text = bundle.getString(Intent.EXTRA_TEXT, "");
            }
        }

        textView = findViewById(R.id.dataTextView);
        textView.setText(text);
    }
}
