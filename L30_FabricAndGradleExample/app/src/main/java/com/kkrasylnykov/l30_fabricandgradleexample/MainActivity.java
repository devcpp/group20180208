package com.kkrasylnykov.l30_fabricandgradleexample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_main);

        findViewById(R.id.error1).setOnClickListener(this);
        findViewById(R.id.error2).setOnClickListener(this);
        findViewById(R.id.error3).setOnClickListener(this);
        findViewById(R.id.error4).setOnClickListener(this);

        Button button = findViewById(R.id.error1);
        button.setText(Constants.TEST_VALUE);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.error1:
                int n = 0;
                int b = 10/n;
                break;
            case R.id.error2:
                int t = 1;
                t *= 5;
                int[] arr = new int[1];
                arr[t] = 100;
                break;
            case R.id.error3:
                Object object = null;
                if (false) {
                    object = new Object();
                }
                object.toString();
                break;
            case R.id.error4:
                throw new RuntimeException("error4!!!!!!");
        }
    }
}
