package com.kkrasylnykov.l24_libsexample.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Contact {
    @Expose(serialize = false, deserialize = false)
    private long _id;

    @Expose
    @SerializedName("id")
    private long serverId;

    @Expose
    private String name;

    @Expose
    private String sname;

    @Expose
    private ArrayList<String> phones;

    @Expose
    private ArrayList<String> address;

    public long get_id() {
        return _id;
    }

    public long getServerId() {
        return serverId;
    }

    public String getName() {
        return name;
    }

    public String getSname() {
        return sname;
    }

    public ArrayList<String> getPhones() {
        return phones;
    }

    public ArrayList<String> getAdress() {
        return address;
    }

    public Contact(){
    }

    public Contact(long id, long serverId, String name, String sname) {
        this._id = id;
        this.serverId = serverId;
        this.name = name;
        this.sname = sname;
    }
}
