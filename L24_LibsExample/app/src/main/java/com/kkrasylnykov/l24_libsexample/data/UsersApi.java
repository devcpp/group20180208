package com.kkrasylnykov.l24_libsexample.data;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface UsersApi {

    @GET("/api/users.{format}")
    Call<ArrayList<Contact>> getAllUsers(@Path("format") String format);

    @POST("/api/users.{format}")
    Call<Contact> addUser(@Path("format") String format, @Body Contact contact);

    @DELETE("/api/users/{userId}.{format}")
    Call<Object> removeUser(@Path("userId") long userId,
                            @Path("format") String format);
}
