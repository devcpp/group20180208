package com.kkrasylnykov.l24_libsexample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.kkrasylnykov.l24_libsexample.data.Contact;
import com.kkrasylnykov.l24_libsexample.data.UsersApi;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    private String data = "{\"id\":1,\"name\":\"t\",\"sname\":\"t\",\"last_update\":\"2018-05-17T01:00:11+0300\",\"phones\":[],\"address\":[]}";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Gson gson = new GsonBuilder()
                .excludeFieldsWithoutExposeAnnotation().create();;

        Contact contact = gson.fromJson(data, Contact.class);

//        Log.d("devcpp", "get_id -> " + contact.get_id());
//        Log.d("devcpp", "getServerId -> " + contact.getServerId());
//        Log.d("devcpp", "getName -> " + contact.getName());
//        Log.d("devcpp", "getSname -> " + contact.getSname());
//        Log.d("devcpp", "getPhones -> " + contact.getPhones());
//        Log.d("devcpp", "getAdress -> " + contact.getAdress());

        Contact contact1 = new Contact(-1, -1, "NameTest", "TestSName");

        String src = gson.toJson(contact1);
        Log.d("devcpp", "src -> " + src);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://46.101.148.28")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        UsersApi usersApi = retrofit.create(UsersApi.class);

        Call<ArrayList<Contact>> call =  usersApi.getAllUsers("json");

        call.enqueue(new Callback<ArrayList<Contact>>() {
            @Override
            public void onResponse(Call<ArrayList<Contact>> call,
                                   Response<ArrayList<Contact>> response) {
                Log.d("devcpp", "response -> " + response.code());
                Log.d("devcpp", "response -> " + response.isSuccessful());
                if (response.code() == 200) {
                    ArrayList<Contact> data = response.body();

                    for (Contact cont : data) {
                        Log.d("devcpp", "get_id -> " + cont.get_id());
                        Log.d("devcpp", "getServerId -> " + cont.getServerId());
                        Log.d("devcpp", "getName -> " + cont.getName());
                        Log.d("devcpp", "getSname -> " + cont.getSname());
                        Log.d("devcpp", "getPhones -> " + cont.getPhones());
                        Log.d("devcpp", "getAdress -> " + cont.getAdress() + "\n\n");
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Contact>> call, Throwable t) {
                Log.d("devcpp","t -> " + t.getMessage());
            }
        });

//        Call<Contact> post = usersApi.addUser("json", contact1);

        usersApi.addUser("json", contact1).enqueue(new Callback<Contact>() {
            @Override
            public void onResponse(Call<Contact> call, Response<Contact> response) {
                Log.d("devcpp", "onResponse -> " + response.code());
            }

            @Override
            public void onFailure(Call<Contact> call, Throwable t) {
                Log.d("devcpp","onFailure -> " + t.getMessage());
            }
        });

        usersApi.removeUser(3, "json").enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                Log.d("devcpp", "removeUser -> onResponse -> " + response.code());
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                Log.d("devcpp","removeUser -> onFailure -> " + t.getMessage());
            }
        });


//        RequestQueue queue = Volley.newRequestQueue(this);
//        String url ="http://46.101.148.28";
//        String getUsersUrl = url + "/api/users.json";

//        StringRequest request = new StringRequest(Request.Method.GET, getUsersUrl, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                Log.d("devcpp", "onResponse -> " + response);
//
//                List<Contact> data = gson.fromJson(response, new TypeToken<List<Contact>>(){}.getType());
//                for (Contact cont : data) {
//                    Log.d("devcpp", "get_id -> " + cont.get_id());
//                    Log.d("devcpp", "getServerId -> " + cont.getServerId());
//                    Log.d("devcpp", "getName -> " + cont.getName());
//                    Log.d("devcpp", "getSname -> " + cont.getSname());
//                    Log.d("devcpp", "getPhones -> " + cont.getPhones());
//                    Log.d("devcpp", "getAdress -> " + cont.getAdress() + "\n\n");
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                Log.d("devcpp", "onErrorResponse -> " + error.getMessage());
//            }
//        });

//        GsonRequest<ArrayData> request =
//                new GsonRequest<>(getUsersUrl, ArrayData.class,
//                null, new Listener<ArrayData>() {
//            @Override
//            public void onResponse(ArrayData response) {
//                for (Contact cont : response) {
//                    Log.d("devcpp", "get_id -> " + cont.get_id());
//                    Log.d("devcpp", "getServerId -> " + cont.getServerId());
//                    Log.d("devcpp", "getName -> " + cont.getName());
//                    Log.d("devcpp", "getSname -> " + cont.getSname());
//                    Log.d("devcpp", "getPhones -> " + cont.getPhones());
//                    Log.d("devcpp", "getAdress -> " + cont.getAdress() + "\n\n");
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                Log.d("devcpp", "onErrorResponse -> " + error.getMessage());
//            }
//        });
//
//        queue.add(request);
    }

    public class ArrayData extends ArrayList<Contact>{
    }
}
