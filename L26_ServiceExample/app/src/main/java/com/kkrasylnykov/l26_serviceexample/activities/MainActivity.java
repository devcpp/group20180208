package com.kkrasylnykov.l26_serviceexample.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.kkrasylnykov.l26_serviceexample.R;

public class MainActivity extends AppCompatActivity
        implements View.OnClickListener {

    private static final String[] DATA =
            {"https://9105.selcdn.ru/devcpp/Video.mp4",
            "https://9105.selcdn.ru/devcpp/000a-Aprenda.mp3",
            "https://9105.selcdn.ru/devcpp/000b-vocales_abecedario.mp3"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.load1).setOnClickListener(this);
        findViewById(R.id.load2).setOnClickListener(this);
        findViewById(R.id.load3).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int position = 0;
        switch (v.getId()) {
            case R.id.load2:
                position = 1;
                break;
            case R.id.load3:
                position = 2;
                break;
        }

        String url = DATA[position];
        startActivity(LoadActivity.getNewIntent(this, url));
    }
}
