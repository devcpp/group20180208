package com.kkrasylnykov.l29_photoandsharedexample;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MainActivity extends AppCompatActivity
        implements View.OnClickListener{

    private static final int REQUEST_IMAGE_CAPTURE = 1001;
    private ImageView imageView;
    private String currentPhotoPath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.cameraBtn).setOnClickListener(this);
        findViewById(R.id.shareBtn).setOnClickListener(this);
        imageView = findViewById(R.id.cameraResultImageView);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cameraBtn:
                showFiles(getFilesDir());
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                File photoFile = null;
                try {
                    photoFile = createImageFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                if (photoFile != null
                        && takePictureIntent.resolveActivity(getPackageManager()) != null) {
                    Uri photoURI = FileProvider.getUriForFile(this,
                            "com.kkrasylnykov.l29_photoandsharedexample.fileprovider",
                            photoFile);
                    Log.d("devcpp_uri", "photoURI -> " + photoURI);
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                    startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                }
                break;
            case R.id.shareBtn:
                if (currentPhotoPath != null) {
                    Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
                    Bitmap imageBitmap = BitmapFactory.decodeFile(currentPhotoPath);

                    int nWidth = imageBitmap.getWidth();
                    int nHeight = imageBitmap.getHeight();

                    Bitmap item = Bitmap.createBitmap(nWidth, nHeight, Bitmap.Config.ARGB_8888);

                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inSampleSize = 10;

                    Bitmap bMap = BitmapFactory.decodeResource(getResources(), R.drawable.watermark, options);

                    int positionX = nWidth - bMap.getWidth() - 16;
                    int positionY = nHeight - bMap.getHeight() - 16;

                    Canvas canvas = new Canvas(item);
                    canvas.drawBitmap(imageBitmap, 0, 0, paint);
                    canvas.drawBitmap(bMap, positionX, positionY, paint);

                    imageView.setImageBitmap(item);
                    File imageFile = new File(currentPhotoPath);
                    if (saveBitmapToFile(item, imageFile)) {
                        Intent shareIntent = new Intent(Intent.ACTION_SEND);
                        shareIntent.setType("image/*");
                        shareIntent.putExtra(Intent.EXTRA_STREAM,
                                FileProvider.getUriForFile(this,
                                        "com.kkrasylnykov.l29_photoandsharedexample.fileprovider",
                                        imageFile));
                        startActivityForResult(Intent.createChooser(shareIntent, "Share Images"),
                                2563);
                    } else {
                    }

                }
                break;
        }

    }

    protected boolean saveBitmapToFile(Bitmap source, File imageFile) {
        try {
            FileOutputStream fos = new FileOutputStream(imageFile);
            source.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            fos.close();
        } catch (FileNotFoundException e) {
            Toast.makeText(this, "Memory error!", Toast.LENGTH_LONG).show();
            e.printStackTrace();
            return false;
        } catch (IOException e) {
            Toast.makeText(this, "Memory error!", Toast.LENGTH_LONG).show();
            e.printStackTrace();
            return false;
        }

        return true;
    }

    private void showFiles(File file){
        Log.d("devcpp_file", file.getAbsolutePath());
        if (file.isDirectory()) {
            File[] files = file.listFiles();
            if (files != null) {
                for (File item : files) {
                    showFiles(item);
                }
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
//            Bundle extras = data.getExtras();
//            Bitmap imageBitmap = (Bitmap) extras.get("data");
            showFiles(getFilesDir());
            Bitmap imageBitmap = BitmapFactory.decodeFile(currentPhotoPath);
            imageView.setImageBitmap(imageBitmap);
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
//        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String timeStamp = Long.toString(System.currentTimeMillis());
        String imageFileName = "JPEG_" + timeStamp + "_";

//        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File storageDir = new File(getFilesDir(), "photos");
        if (!storageDir.exists()) {
            storageDir.mkdir();
        }
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        currentPhotoPath = image.getAbsolutePath();
        return image;
    }
}
