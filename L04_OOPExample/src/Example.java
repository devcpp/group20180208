public class Example {
	public static final String KEY_FOR_DATA = "KEY_FOR_DATA";
	
	public static int count;
	
	private int val = 100;
	
	static {
		if (System.currentTimeMillis() % 2 == 0) {
			count = 10;
		} else {
			count = 100;
		}
	}
	
	public Example() {
		System.out.println(++count);
	}
	
	public static int getSize() {
		return KEY_FOR_DATA.length() * count;
	}
	
}
