package deta;

import java.util.Scanner;

public class Moto extends BaseTransport {
	
	String legRestraints;
	

	public String getLegRestraints() {
		return legRestraints;
	}
	
	public void setLegRestraints(String legRestraints) {
		this.legRestraints = legRestraints;
	}

	@Override
	public void inputData() {
		Scanner scanner = new Scanner(System.in);
		System.out.print("Enter moto trade name: ");
		String tradeName = scanner.nextLine();
		
		System.out.print("Enter moto model name: ");
		String modelName = scanner.nextLine();
		
		int wheelCount = 2;
		
		System.out.print("Enter moto year old: ");
		int yearOld = scanner.nextInt();
		
		scanner.nextLine(); //Need for correct work after enter number
		System.out.print("Enter moto leg restraints: ");
		String legRestraints = scanner.nextLine();
		
		setTradeName(tradeName);
		setModelName(modelName);
		setWheelCount(wheelCount);
		setYearOld(yearOld);
		setLegRestraints(legRestraints);
		
	}

	@Override
	public void outputData() {
		System.out.println("Moto " + getTradeName() + " " + getModelName()
		+ " color " + getYearOld() + " year with " + getLegRestraints());
		
	}

}
