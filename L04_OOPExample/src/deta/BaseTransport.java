package deta;

public abstract class BaseTransport {
	
	private String tradeName;
	private String modelName;
	private int wheelCount;
	private int yearOld;
	
	public String getTradeName() {
		return tradeName;
	}
	
	public void setTradeName(String tradeName) {
		this.tradeName = tradeName;
	}
	
	public String getModelName() {
		return modelName;
	}
	
	public void setModelName(String modelName) {
		this.modelName = modelName;
	}
	
	public int getWheelCount() {
		return wheelCount;
	}
	
	public void setWheelCount(int wheelCount) {
		this.wheelCount = wheelCount;
	}
	
	public int getYearOld() {
		return yearOld;
	}
	
	public void setYearOld(int yearOld) {
		this.yearOld = yearOld;
	}
	
	public abstract void inputData();
	
	public abstract void outputData();
	
	public boolean search(String str) {
		str = str.toLowerCase();
		boolean result = getTradeName().toLowerCase().contains(str)
				|| getModelName().toLowerCase().contains(str)
				|| Integer.toString(getYearOld()).contains(str);
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		boolean result = false;
		
		if (obj instanceof String) {
			result = this.search((String)obj);
		}
		
		return result;
	}

}
