package deta;

import java.util.Scanner;

public class Car extends BaseTransport {
	
	private String color;
	private String bodyTupe;
	
	
	public String getColor() {
		return color;
	}
	
	public void setColor(String color) {
		this.color = color;
	}
	
	public String getBodyTupe() {
		return bodyTupe;
	}
	
	public void setBodyTupe(String bodyTupe) {
		this.bodyTupe = bodyTupe;
	}

	@Override
	public void inputData() {
		Scanner scanner = new Scanner(System.in);
		System.out.print("Enter car trade name: ");
		String tradeName = scanner.nextLine();
		
		System.out.print("Enter car model name: ");
		String modelName = scanner.nextLine();
		
		System.out.print("Enter car body type: ");
		String bodyTupe = scanner.nextLine();
		
		System.out.print("Enter car wheel count: ");
		int wheelCount = scanner.nextInt();
		
		System.out.print("Enter car year old: ");
		int yearOld = scanner.nextInt();
		
		scanner.nextLine(); //Need for correct work after enter number
		System.out.print("Enter car color: ");
		String color = scanner.nextLine();
		
		setTradeName(tradeName);
		setModelName(modelName);
		setWheelCount(wheelCount);
		setYearOld(yearOld);
		setBodyTupe(bodyTupe);
		setColor(color);
		
	}

	@Override
	public void outputData() {
		System.out.println("Car " + getTradeName() + " " + getModelName()
		+ " body " + getBodyTupe() + " " + getColor() + " color " + getYearOld() + " year");
		
	}
	
	@Override
	public boolean search(String str) {
		boolean result = super.search(str) || getBodyTupe().toLowerCase().contains(str)
				|| getColor().toLowerCase().contains(str);
		return result;
	}

}
