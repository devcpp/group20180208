import java.util.ArrayList;
import java.util.Scanner;

import deta.BaseTransport;
import deta.Car;
import deta.Moto;

public class Main {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		ArrayList<BaseTransport> data = new ArrayList<>();
		
		int i = 0;
		do {
			System.out.println("Menu:");	
			System.out.println("1. Add");
			System.out.println("2. Show all");
			System.out.println("3. Search");
			System.out.println("4. Remove all");
			System.out.println("0. Exit");
			
			i = scanner.nextInt();
			switch (i) {
				case 1:
					System.out.println("Menu:");	
					System.out.println("1. Car");
					System.out.println("2. Moto");
					
					i = scanner.nextInt();
					BaseTransport item;
					if (i == 1) {
						item = new Car();
					} else {
						item = new Moto();
					}
					item.inputData();
					data.add(item);
					break;
				case 2:
					for (BaseTransport outItem : data) {
						outItem.outputData();
						if (outItem instanceof Car) {
							System.out.println(" -> " + outItem.getWheelCount());
						}
					}
					break;
				case 3:
					scanner.nextLine();
					System.out.print("Enter search: ");
					String strSearch = scanner.nextLine();
					//for (BaseTransport outItem : data) {
					//	if (outItem.search(strSearch)) {
					//		outItem.outputData();
					//	}
					//}
					for (BaseTransport outItem : data) {
						if (outItem.equals(strSearch)) {
							outItem.outputData();
						}
					}
					
					break;
				case 4:
					data.clear();
					break;
			}
		} while (i != 0);
	}

}
