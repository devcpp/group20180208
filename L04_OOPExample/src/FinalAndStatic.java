import java.util.ArrayList;

public class FinalAndStatic {
	

	public static void main(String[] args) {
		
		 final ArrayList<String> data = new ArrayList<>();
		 data.add("Test");
		 
		 //data = new ArrayList<>();
		 
		 final int a = 10;
		 
		 //a = 30;
		 
		 System.out.println(Example.KEY_FOR_DATA);
		 
		 Example.count = 10;
		 
		 new Example();
		 new Example();
		 new Example();
		 new Example();
		 
		 
	}

}
