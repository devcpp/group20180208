package com.kkrasylnykov.l10_dialogsexample;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class CustomFragmentDialog extends DialogFragment {

//    @Nullable
//    @Override
//    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        View root = inflater.inflate(R.layout.dialog_custom, container, false);
//        return root;
//    }


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog alertDialog;
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
//                AlertDialog alertDialog = builder.create();
//                alertDialog.show();
        builder.setTitle("Warning!!!");
        builder.setMessage("Realy???");
        builder.setPositiveButton("Ok", null);
        return builder.create();
    }
}
