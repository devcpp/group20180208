package com.kkrasylnykov.l10_dialogsexample;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TimePicker;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.alertDialog).setOnClickListener(this);
        findViewById(R.id.progressDialog).setOnClickListener(this);
        findViewById(R.id.timeDialog).setOnClickListener(this);
        findViewById(R.id.dateDialog).setOnClickListener(this);
        findViewById(R.id.customDialog).setOnClickListener(this);
        findViewById(R.id.fragmentDialog).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.alertDialog:
                AlertDialog alertDialog;
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
//                AlertDialog alertDialog = builder.create();
//                alertDialog.show();
                builder.setTitle("Warning!!!");
                builder.setMessage("Realy???");
                builder.setPositiveButton("Positive", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(MainActivity.this, "click -> PositiveButton", Toast.LENGTH_LONG).show();
                    }
                });

                builder.setNegativeButton("Negative", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(MainActivity.this, "click -> Negative", Toast.LENGTH_LONG).show();
                    }
                });

                builder.setNeutralButton("Neutral", null);

                builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        Toast.makeText(MainActivity.this, "click -> setOnCancelListener", Toast.LENGTH_LONG).show();
                    }
                });

                builder.setCancelable(false);

                alertDialog = builder.show();
//                AlertDialog alertDialog = new AlertDialog(this);
                break;
            case R.id.progressDialog:
                final ProgressDialog progressDialog = new ProgressDialog(this);
                progressDialog.setMessage("Please wait....");
                progressDialog.setCancelable(false);
                progressDialog.show();

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        progressDialog.dismiss();
                    }
                }, 10000);
                break;
            case R.id.timeDialog:
                TimePickerDialog timePickerDialog = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        Log.d("devcpp", "hourOfDay -> " + hourOfDay + " -> minute -> " + minute);
                    }
                }, 20, 37, true);
                timePickerDialog.show();
                break;
            case R.id.dateDialog:
                DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        Log.d("devcpp", "year -> " + year + " -> month -> " + month + " -> dayOfMonth -> " + dayOfMonth);
                    }
                }, 2018, 3, 27);
                datePickerDialog.show();
                break;
            case R.id.customDialog:
                CustomDialog customDialog = new CustomDialog(this);
                customDialog.show();
                break;
            case R.id.fragmentDialog:
                CustomFragmentDialog customFragmentDialog = new CustomFragmentDialog();
                customFragmentDialog.show(getSupportFragmentManager(), "CustomFragmentDialog");
                break;
        }
    }
}
