package com.kkrasylnykov.l20_filesandpermisionexamples.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Display;
import android.widget.ImageView;

import com.kkrasylnykov.l20_filesandpermisionexamples.R;
import com.kkrasylnykov.l20_filesandpermisionexamples.adapters.ImageViewPagerFragmentAdapter;

import java.io.File;
import java.util.ArrayList;

public class ViewImageActivity extends AppCompatActivity {

    public static final String KEY_FILE_PATHS = "KEY_FILE_PATHS";
    public static final String KEY_POSITION = "KEY_POSITION";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_image);

        ArrayList<String> arrData = null;
        int nPosition = 0;
        Bundle bundle = getIntent().getExtras();
        if (bundle!=null){
            arrData = (ArrayList<String>) bundle.get(KEY_FILE_PATHS);
            nPosition = bundle.getInt(KEY_POSITION, 0);
        }

        if (arrData == null || arrData.size()==0){
            finish();
        }

        ViewPager viewPager = (ViewPager) findViewById(R.id.ViewPager);
        ImageViewPagerFragmentAdapter adapter = new ImageViewPagerFragmentAdapter(getSupportFragmentManager(),arrData);
        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(nPosition);
    }
}
