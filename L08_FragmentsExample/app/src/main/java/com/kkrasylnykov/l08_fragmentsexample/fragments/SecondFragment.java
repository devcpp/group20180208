package com.kkrasylnykov.l08_fragmentsexample.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kkrasylnykov.l08_fragmentsexample.R;

public class SecondFragment extends Fragment {
    TextView textView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_second,
                container, false);
        textView = rootView.findViewById(R.id.textSecondFragment);
        return rootView;
    }

    public void setFragmentText(String text){
        textView.setText(text);
    }
}
