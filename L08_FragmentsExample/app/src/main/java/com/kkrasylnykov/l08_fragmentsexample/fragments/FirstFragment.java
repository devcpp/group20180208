package com.kkrasylnykov.l08_fragmentsexample.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Parcel;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.kkrasylnykov.l08_fragmentsexample.R;
import com.kkrasylnykov.l08_fragmentsexample.activities.MainActivity;

public class FirstFragment extends Fragment
        implements View.OnClickListener {

    private EditText editText;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("devcpp","onCreate");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        Log.d("devcpp","onCreateView");
        View rootView = inflater.inflate(R.layout.fragment_first,
                container, false);

        rootView.findViewById(R.id.btnFirstFragment).setOnClickListener(this);
        editText = rootView.findViewById(R.id.editTextFirstFragment);
        return rootView;
    }

    @Override
    public void onClick(View v) {
        Activity activity = getActivity();
        if (activity instanceof MainActivity) {
            MainActivity mainActivity = (MainActivity) activity;
            mainActivity.setText(editText.getText().toString());
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        Log.d("devcpp","onActivityCreated");
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        Log.d("devcpp","onAttach");
        super.onAttach(context);
    }

    @Override
    public void onStart() {
        Log.d("devcpp","onStart");
        super.onStart();
    }

    @Override
    public void onResume() {
        Log.d("devcpp","onResume");
        super.onResume();
    }

    @Override
    public void onDestroyView() {
        Log.d("devcpp","onDestroyView");
        super.onDestroyView();
    }

    @Override
    public void onStop() {
        Log.d("devcpp","onStop");
        super.onStop();
    }

    @Override
    public void onPause() {
        Log.d("devcpp","onPause");
        super.onPause();
    }

    @Override
    public void onDestroy() {
        Log.d("devcpp","onDestroy");
        super.onDestroy();
    }
}
