package com.kkrasylnykov.l08_fragmentsexample.activities;

import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.kkrasylnykov.l08_fragmentsexample.R;
import com.kkrasylnykov.l08_fragmentsexample.fragments.FirstFragment;
import com.kkrasylnykov.l08_fragmentsexample.fragments.SecondFragment;

public class MainActivity extends AppCompatActivity
        implements View.OnClickListener {

    private FirstFragment firstFragment;
    private SecondFragment secondFragment;

    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        firstFragment = new FirstFragment();
        secondFragment = new SecondFragment();

//        textView = findViewById(R.id.textMainActivity);

        FragmentTransaction ft = getSupportFragmentManager()
                .beginTransaction();
        ft.add(R.id.firstFrameLayout, firstFragment);
        ft.add(R.id.secondFrameLayout, secondFragment);
        ft.commit();
//        findViewById(R.id.addFragmetn).setOnClickListener(this);
//        findViewById(R.id.removeFragmetn).setOnClickListener(this);
//        findViewById(R.id.addFragmetn2).setOnClickListener(this);
//        findViewById(R.id.removeFragmetn2).setOnClickListener(this);
    }

    public void setText(String text){
//        textView.setText(text);
        secondFragment.setFragmentText(text);
    }

    @Override
    public void onClick(View v) {
//        FragmentTransaction ft = getSupportFragmentManager()
//                .beginTransaction();
//        switch (v.getId()){
//            case R.id.addFragmetn:
//                if(!firstFragment.isAdded()) {
//                    ft.add(R.id.firstFrameLayout, firstFragment);
//                } else {
//                    ft.show(firstFragment);
//                }
////                ft.replace(R.id.firstFrameLayout, firstFragment);
//                break;
//            case R.id.removeFragmetn:
//                ft.remove(firstFragment);
//                break;
//            case R.id.addFragmetn2:
//                if(!secondFragment.isAdded()) {
//                    ft.add(R.id.firstFrameLayout, secondFragment);
//                }
////                ft.replace(R.id.firstFrameLayout, secondFragment);
//                break;
//            case R.id.removeFragmetn2:
//                ft.remove(secondFragment);
//                break;
//        }
//        if (!getSupportFragmentManager().getFragments().isEmpty()){
//            ft.addToBackStack("addToBackStack");
//        }
//        ft.commit();
    }
}
