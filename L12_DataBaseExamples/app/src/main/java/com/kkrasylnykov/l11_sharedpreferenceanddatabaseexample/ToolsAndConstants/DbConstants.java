package com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.ToolsAndConstants;

public class DbConstants {
    public static final String DB_NAME = "notepad_db";
    public static final int DB_VERSION = 1;

    public static final String TABLE_NAME = "_UserInfo";
    public static final String FIELD_ID = "_id";
    public static final String FIELD_NAME = "_name";
    public static final String FIELD_SNAME = "_sname";
    public static final String FIELD_PHONE = "_phone";
    public static final String FIELD_ADRESS = "_adress";

    AppSettings appSettings = new AppSettings(null);
}
