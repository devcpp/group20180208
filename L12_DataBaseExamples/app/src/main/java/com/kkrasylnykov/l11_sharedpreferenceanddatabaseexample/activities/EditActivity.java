package com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.activities;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.R;
import com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.ToolsAndConstants.DbConstants;
import com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.db.DataBaseHelper;

public class EditActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String USER_ID = "USER_ID";

    private EditText nameEditText;
    private EditText snameEditText;
    private EditText phoneEditText;
    private EditText adressEditText;

    private long userId = -1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            userId = bundle.getLong(USER_ID, -1);
        }

        nameEditText = findViewById(R.id.nameEditTextEditActivity);
        snameEditText = findViewById(R.id.snameEditTextEditActivity);
        phoneEditText = findViewById(R.id.phoneEditTextEditActivity);
        adressEditText = findViewById(R.id.adressEditTextEditActivity);

        Button addButton = findViewById(R.id.addBtnEditActivity);
        addButton.setOnClickListener(this);

        if (userId != -1) {
            DataBaseHelper dataBaseHelper = new DataBaseHelper(this);
            SQLiteDatabase db = dataBaseHelper.getReadableDatabase();

            String selection = DbConstants.FIELD_ID + "=?";
            String[] selectionArgs = new String[]{Long.toString(userId)};

            Cursor cursor = db.query(DbConstants.TABLE_NAME, null,
                    selection, selectionArgs,
                    null, null, null);
            if (cursor != null) {
                int namePosition = cursor.getColumnIndex(DbConstants.FIELD_NAME);
                int snamePosition = cursor.getColumnIndex(DbConstants.FIELD_SNAME);
                int phonePosition = cursor.getColumnIndex(DbConstants.FIELD_PHONE);
                int adressPosition = cursor.getColumnIndex(DbConstants.FIELD_ADRESS);
                if (cursor.moveToFirst()) {
                    nameEditText.setText(cursor.getString(namePosition));
                    snameEditText.setText(cursor.getString(snamePosition));
                    phoneEditText.setText(cursor.getString(phonePosition));
                    adressEditText.setText(cursor.getString(adressPosition));

                    addButton.setText("Update");

                    Button removeButton = findViewById(R.id.removeBtnEditActivity);
                    removeButton.setOnClickListener(this);
                    removeButton.setVisibility(View.VISIBLE);
                }

                cursor.close();
            }
            db.close();

        }
    }

    @Override
    public void onClick(View v) {
        DataBaseHelper dataBaseHelper = new DataBaseHelper(this);
        SQLiteDatabase db = dataBaseHelper.getWritableDatabase();
        String selection = DbConstants.FIELD_ID + "=?";
        String[] selectionArgs = new String[]{Long.toString(userId)};
        switch (v.getId()){
            case R.id.removeBtnEditActivity:
                db.delete(DbConstants.TABLE_NAME, selection, selectionArgs);

                finish();
                break;
            case R.id.addBtnEditActivity:
                String name = nameEditText.getText().toString();
                String sname = snameEditText.getText().toString();
                String phone = phoneEditText.getText().toString();
                String adress = adressEditText.getText().toString();

                if (name != null && !name.isEmpty()) {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(DbConstants.FIELD_NAME, name);
                    contentValues.put(DbConstants.FIELD_SNAME, sname);
                    contentValues.put(DbConstants.FIELD_PHONE, phone);
                    contentValues.put(DbConstants.FIELD_ADRESS, adress);

                    if (userId != -1) {
                        db.update(DbConstants.TABLE_NAME, contentValues, selection, selectionArgs);
                    } else {
                        db.insert(DbConstants.TABLE_NAME, null, contentValues);
                    }

                    if (userId != -1) {
                        finish();
                    } else {
                        nameEditText.setText("");
                        snameEditText.setText("");
                        phoneEditText.setText("");
                        adressEditText.setText("");

                        nameEditText.requestFocus();
                    }

                } else {
                    Toast.makeText(this, "Need field name not null or empty!", Toast.LENGTH_LONG).show();
                }
                break;
        }

        db.close();
    }
}
