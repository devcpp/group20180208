package com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.activities;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.R;
import com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.ToolsAndConstants.AppSettings;
import com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.ToolsAndConstants.DbConstants;
import com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.db.DataBaseHelper;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashSet;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int REQUEST_CODE_TC = 1001;

    private LinearLayout conteinerLinearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        HashSet<Float> arr = new HashSet<>();

        AppSettings appSettings = new AppSettings(this);
        if (appSettings.isNeedShowTC()) {
            Intent intent = new Intent(this, TandCActivity.class);
            startActivityForResult(intent, REQUEST_CODE_TC);

            DataBaseHelper dataBaseHelper = new DataBaseHelper(this);
            SQLiteDatabase db = dataBaseHelper.getWritableDatabase();

            for (int i=0; i<10; i++) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(DbConstants.FIELD_NAME, "Name" + i);
                contentValues.put(DbConstants.FIELD_SNAME, "SName" + i);
                contentValues.put(DbConstants.FIELD_PHONE, "09" + i + "052465" + i*10);
                contentValues.put(DbConstants.FIELD_ADRESS, "str. Devichya " + i);
                long id = db.insert(DbConstants.TABLE_NAME, null, contentValues);

                Log.d("devcpp", "insert -> id -> " + id);
            }
            db.close();
        }

        conteinerLinearLayout = findViewById(R.id.conteinerLinearLayout);
        findViewById(R.id.addBtnMainActivity).setOnClickListener(this);
        findViewById(R.id.removeAllBtnMainActivity).setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateDataOnScreen();
    }

    private void updateDataOnScreen(){
        conteinerLinearLayout.removeAllViews();

        DataBaseHelper dataBaseHelper = new DataBaseHelper(this);
        SQLiteDatabase db = dataBaseHelper.getReadableDatabase();

        Cursor cursor = db.query(DbConstants.TABLE_NAME, null, null, null,
                null, null, null);

        if (cursor != null) {
            int idPosition = cursor.getColumnIndex(DbConstants.FIELD_ID);
            int namePosition = cursor.getColumnIndex(DbConstants.FIELD_NAME);
            int snamePosition = cursor.getColumnIndex(DbConstants.FIELD_SNAME);
            int phonePosition = cursor.getColumnIndex(DbConstants.FIELD_PHONE);
            int adressPosition = cursor.getColumnIndex(DbConstants.FIELD_ADRESS);
            if (cursor.moveToFirst()) {
                LinearLayout.LayoutParams params =
                        new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                                ViewGroup.LayoutParams.WRAP_CONTENT);

                int topAndBottomMargin = (int) getResources()
                        .getDimension(R.dimen.top_and_bottom_margin_for_list);
                do{
                    long id = cursor.getLong(idPosition);
                    String name = cursor.getString(namePosition);
                    String sname = cursor.getString(snamePosition);
                    String phone = cursor.getString(phonePosition);
                    String adress = cursor.getString(adressPosition);

                    TextView textView = new TextView(this);

                    params.setMargins(0, topAndBottomMargin, 0, topAndBottomMargin);

                    textView.setLayoutParams(params);
//                    textView.setText(name + " " + sname + "\n" + phone + "\n" + adress);
                    textView.setText(MessageFormat.format("'{'{0} {1}'}'\n{2}\n{0} - {3}",
                            name, sname, phone, adress));
                    textView.setTag(id);
                    textView.setOnClickListener(this);
                    conteinerLinearLayout.addView(textView);
                } while(cursor.moveToNext());
            }
            cursor.close();
        }
        db.close();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_TC) {
            AppSettings appSettings = new AppSettings(this);
            if (appSettings.isNeedShowTC()) {
                Toast.makeText(this, "Вы не приняли соглашение!(((", Toast.LENGTH_LONG).show();
                finish();
            }
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getTag() != null && v.getTag() instanceof Long) {
            long id = (long) v.getTag();
            Intent intent = new Intent(this, EditActivity.class);
            intent.putExtra(EditActivity.USER_ID, id);
            startActivity(intent);
        } else {
            switch (v.getId()) {
                case R.id.addBtnMainActivity:
                    startActivity(new Intent(this, EditActivity.class));
                    break;
                case R.id.removeAllBtnMainActivity:
                    DataBaseHelper dataBaseHelper = new DataBaseHelper(this);
                    SQLiteDatabase db = dataBaseHelper.getWritableDatabase();
                    db.delete(DbConstants.TABLE_NAME, null, null);
                    db.close();
                    updateDataOnScreen();
                    break;
            }
        }

    }
}
