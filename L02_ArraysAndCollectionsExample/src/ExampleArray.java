import java.util.Scanner;

public class ExampleArray {

	public static void main(String[] args) {
		//int[] mas, mas1;
		//int mas2[], mas3 = 15;
		
		//int[][] mas4;
		
		//mas = new int[10];
		//mas1 = new int[mas3];
		//mas2 = new int[] {2, 3, 5};
		
		//mas2[2] = 10;
		
		Scanner scanner = new Scanner(System.in);
		
		int count = 0;
		while (true) {
			System.out.print("Enter count: ");
			count = scanner.nextInt();
			
			if (count > 0) {
				break;
			}
			System.out.println("Error data!!!!");
		}
		
		
		float[] arrL = new float[count];
		
		for (int i = 0; i < arrL.length; i++) {
			float data = 0f;
			boolean isNeedBreak = false;
			while (true) {
				System.out.print("Enter " + (i + 1) + ": ");
				data = scanner.nextFloat();
				
				if (data == 0) {
					isNeedBreak = true;
					break;
				} else if (data > 0) {
					break;
				}
				System.out.println("Error data!!!!");
			}
			if (isNeedBreak) {
				break;
			}
			
			arrL[i] = data;
		}
		
		float p = 0;
		for (float data : arrL) {
			p+=data;
		}
		
		System.out.println("P = " + p);
	}

}
