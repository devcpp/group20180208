import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;

public class ListExample {

	public static void main(String[] args) {
		
		//ArrayList<Integer> listData = new ArrayList<>(30);
		LinkedList<Integer> listData = new LinkedList<>();
		
		listData.add(10);
		listData.add(0, 20);
		listData.add(1, 20);
		int n = listData.get(0);
		listData.set(0, 40);
		listData.contains(30);
		
		Integer d = 20;
		int f = listData.remove(0);
		boolean b = listData.remove(d);
		
		//HashSet<Integer> set = new HashSet<>();
		LinkedHashSet<Integer> set = new LinkedHashSet<>();
		
		System.out.println("set.add(30) -> " + set.add(30));
		System.out.println("set.add(40) -> " + set.add(40));
		System.out.println("set.add(30) -> " + set.add(30));
		System.out.println("set.add(32) -> " + set.add(32));
		System.out.println("set.contains(30) -> " + set.contains(30));
		System.out.println("set.remove(30) -> " + set.remove(30));
		System.out.println("set.contains(30) -> " + set.contains(30));
		
		for (Integer data : set) {
			System.out.println(data);
		}
		
		
		
	}

}
