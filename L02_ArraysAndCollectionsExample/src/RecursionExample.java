import java.util.Scanner;

public class RecursionExample {

	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		
		int n = scanner.nextInt();
		int m = scanner.nextInt();
		
		System.out.println(pow(n, m));
	}
	
	public static long pow(int val, int pow) {
		long result = 1;
		if (pow != 0) {
			result = val * pow(val, pow - 1);
		}
		return result;
	}

}
