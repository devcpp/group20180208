package com.kkrasylnykov.l26_serviceexample.services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class LoadFileService extends Service {

    private static final String KEY_SERVICE_URL = "URL";
    public static final String ACTION_LOAD = "com.kkrasylnykov.l26_serviceexample.services.LoadFileService.PROGRESS";
    public static final String KEY_COUNT = "Count";

    public static Intent getNewIntent(Context context, String url){
        Intent intent = new Intent(context, LoadFileService.class);
        intent.putExtra(KEY_SERVICE_URL, url);
        return intent;
    }

    private ArrayList<String> urls;

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d("devcpp","onCreate -> ");
        urls = new ArrayList<>();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        String url = null;
        if (intent != null) {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                url = bundle.getString(KEY_SERVICE_URL, null);
            }
        }

        if (url == null) {
            return START_STICKY;
        }

        if (!urls.contains(url)) {
            urls.add(url);

            final String curUrl = url;
            Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                InputStream is = null;
                FileOutputStream fos = null;
                try {
                    URL url = new URL(curUrl);
                    is = url.openStream();

                    DataInputStream dis = new DataInputStream(is);

                    byte[] buffer = new byte[2048];
                    int length;
                    long totalLength = 0;

                    File dir = new File(Environment.getExternalStorageDirectory() +
                            "/data_load/");

                    if (!dir.exists()) {
                        dir.mkdir();
                    }

                    fos = new FileOutputStream(
                            new File(dir,
                                    "file" + System.currentTimeMillis() + ".load"));
                    while ((length = dis.read(buffer))>0) {
                        fos.write(buffer, 0, length);
                        totalLength += length;
                        Log.d("devcpp", curUrl + " -> " + totalLength);
                        Intent bcIntent = new Intent();
                        bcIntent.setAction(ACTION_LOAD + curUrl);
                        bcIntent.putExtra(KEY_COUNT, totalLength);
                        sendBroadcast(bcIntent);
                    }
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    if (e != null) {
                        e.printStackTrace();
                    }
                } finally {
                    if (is != null) {
                        try {
                            is.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    if (fos != null) {
                        try {
                            fos.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }

            }
        });
        thread.start();
        }

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        Log.d("devcpp","onDestroy -> ");
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
