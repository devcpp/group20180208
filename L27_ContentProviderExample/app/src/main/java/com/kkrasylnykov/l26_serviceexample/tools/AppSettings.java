package com.kkrasylnykov.l26_serviceexample.tools;


import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.preference.PreferenceManager;

import com.kkrasylnykov.l26_serviceexample.providers.SharedPrefProvider;

public class AppSettings {
    private static final String KEY_INT_COUNT_LOAD = "KEY_INT_COUNT_LOAD";

    private Context context;

    public AppSettings(Context context){
        this.context = context;
    }

    public int getCount(){
        Cursor cursor = context.getContentResolver().query(SharedPrefProvider.COUNT_LOAD_URI,
                null, null, null, null);
        int count = 0;
        if (cursor != null) {
            if(cursor.moveToFirst()) {
                count = cursor.getInt(cursor.getColumnIndex(SharedPrefProvider.KEY_DATA));
            }
            cursor.close();
        }
        return count;
    }

    public int incCount(){
        int count = getCount() + 1;
        ContentValues contentValues = new ContentValues();
        contentValues.put(SharedPrefProvider.KEY_DATA, count);
        context.getContentResolver().insert(SharedPrefProvider.COUNT_LOAD_URI, contentValues);
        return count;
    }
}
