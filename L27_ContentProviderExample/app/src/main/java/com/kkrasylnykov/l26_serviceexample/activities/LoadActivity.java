package com.kkrasylnykov.l26_serviceexample.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import com.kkrasylnykov.l26_serviceexample.R;
import com.kkrasylnykov.l26_serviceexample.services.LoadFileService;
import com.kkrasylnykov.l26_serviceexample.tools.AppSettings;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

public class LoadActivity extends AppCompatActivity {

    private static final String KEY_URL = "KEY_URL";

    public static Intent getNewIntent(Context context, String url){
        Intent intent = new Intent(context, LoadActivity.class);
        intent.putExtra(KEY_URL, url);
        return intent;
    }

    private String url;
    private TextView urlTextView;

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                Bundle bundle = intent.getExtras();
                if (bundle != null && urlTextView != null) {
                    urlTextView.setText(url + " -> " + bundle.getLong(LoadFileService.KEY_COUNT));
                }
            }
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_load);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            url = bundle.getString(KEY_URL, null);
        }

        if (url == null) {
            finish();
            return;
        }

        urlTextView = findViewById(R.id.urlTextView);
        urlTextView.setText(url);

        AppSettings settings = new AppSettings(this);
        Log.d("devcpp", "LoadActivity -> " + settings.incCount());

        //startService(LoadFileService.getNewIntent(this, url));
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(broadcastReceiver, new IntentFilter(LoadFileService.ACTION_LOAD + url));
    }

    @Override
    protected void onPause() {
        unregisterReceiver(broadcastReceiver);
        super.onPause();
    }
}
