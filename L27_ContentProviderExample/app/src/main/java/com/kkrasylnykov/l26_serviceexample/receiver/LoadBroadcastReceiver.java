package com.kkrasylnykov.l26_serviceexample.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.kkrasylnykov.l26_serviceexample.services.StatisticService;

public class LoadBroadcastReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("devcpp", "LoadBroadcastReceiver -> ");
        context.startService(StatisticService.getNewIntent(context));
    }
}
