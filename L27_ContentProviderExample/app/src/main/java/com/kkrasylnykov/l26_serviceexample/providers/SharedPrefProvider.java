package com.kkrasylnykov.l26_serviceexample.providers;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

public class SharedPrefProvider extends ContentProvider {

    public static final String KEY_DATA = "KEY_DATA";

    private static final String AUTHORITIES = "com.kkrasylnykov.l26_serviceexample.providers.SharedPrefProvider";

    private static final String KEY_INT_COUNT_LOAD = "KEY_INT_COUNT_LOAD";

    private static final String COUNT_LOAD = KEY_INT_COUNT_LOAD.toLowerCase();

    public static final Uri COUNT_LOAD_URI = Uri.parse("content://" + AUTHORITIES + "/" + COUNT_LOAD);

    private SharedPreferences sharedPreferences;

    @Override
    public boolean onCreate() {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        return true;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection,
                        @Nullable String selection, @Nullable String[] selectionArgs,
                        @Nullable String sortOrder) {
        Log.d("devcpp", "SharedPrefProvider ->  query -> " + uri);
        Object data = null;
        if (uri.getLastPathSegment().equals(COUNT_LOAD)) {
            data = sharedPreferences.getInt(KEY_INT_COUNT_LOAD, 0);
        }

        MatrixCursor cursor = new MatrixCursor(new String[]{KEY_DATA}, 1);
        cursor.addRow(new Object[]{data});

        return cursor;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        Log.d("devcpp", "SharedPrefProvider ->  insert -> " + uri);
        if (values != null) {
            if (uri.getLastPathSegment().equals(COUNT_LOAD)) {
                int count = values.getAsInteger(KEY_DATA);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putInt(KEY_INT_COUNT_LOAD, count);
                editor.commit();
            }
        }
        return uri;
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection,
                      @Nullable String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values,
                      @Nullable String selection, @Nullable String[] selectionArgs) {
        return 0;
    }
}
