package com.kkrasylnykov.l26_serviceexample.services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.kkrasylnykov.l26_serviceexample.tools.AppSettings;

public class StatisticService extends Service {

    private Thread thread ;

    public static Intent getNewIntent(Context context){
        return new Intent(context, StatisticService.class);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (thread == null) {
            thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    AppSettings settings = new AppSettings(StatisticService.this);
                    while (true) {
                        Log.d("devcpp", "StatisticService -> " + settings.getCount());
                        try {
                            Thread.sleep(2000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }

                }
            });
            thread.start();
        }
        return START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
