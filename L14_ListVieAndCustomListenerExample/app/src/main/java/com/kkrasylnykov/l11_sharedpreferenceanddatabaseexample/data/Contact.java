package com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.data;

public class Contact {

    long id;
    String name;
    String sname;
    String phone;
    String adress;

    public Contact(String name, String sname, String phone, String adress) {
        this(-1, name, sname, phone, adress);
    }

    public Contact(long id, String name, String sname, String phone, String adress) {
        this.id = id;
        this.name = name;
        this.sname = sname;
        this.phone = phone;
        this.adress = adress;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSname() {
        return sname;
    }

    public void setSname(String sname) {
        this.sname = sname;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }
}
