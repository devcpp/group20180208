package com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.engines;

import android.content.Context;

import com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.data.Contact;
import com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.providers.db.ContactDBProvider;

import java.util.ArrayList;

public class ContactEngine {

    Context context;

    public ContactEngine(Context context) {
        this.context = context;
    }

    public ArrayList<Contact> getAll(){
        ContactDBProvider dbProvider = new ContactDBProvider(context);
        return dbProvider.getAll();
    }

    public Contact getItemById(long id){
        ContactDBProvider dbProvider = new ContactDBProvider(context);
        return dbProvider.getItemById(id);
    }

    public void insetrItem(Contact contact){
        ContactDBProvider dbProvider = new ContactDBProvider(context);
        dbProvider.insetrItem(contact);
    }

    public void updateItem(Contact contact){
        ContactDBProvider dbProvider = new ContactDBProvider(context);
        dbProvider.updateItem(contact);
    }

    public void removeItem(Contact contact){
        removeItemById(contact.getId());
    }

    public void removeItemById(long id){
        ContactDBProvider dbProvider = new ContactDBProvider(context);
        dbProvider.removeItemById(id);
    }

    public void removeAll(){
        ContactDBProvider dbProvider = new ContactDBProvider(context);
        dbProvider.removeAll();
    }
}
