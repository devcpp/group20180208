package com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.ToolsAndConstants;


import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class AppSettings {
    private static final String KEY_BOOLEAN_IS_NEED_SHOW_TC = "KEY_BOOLEAN_IS_NEED_SHOW_TC";

    private SharedPreferences sharedPreferences;

    public AppSettings(Context context){
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public void setIsNeedShowTC(boolean isNeedShowTC){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(KEY_BOOLEAN_IS_NEED_SHOW_TC, isNeedShowTC);
        editor.commit();
    }

    public boolean isNeedShowTC(){
        return sharedPreferences.getBoolean(KEY_BOOLEAN_IS_NEED_SHOW_TC, true);
    }
}
