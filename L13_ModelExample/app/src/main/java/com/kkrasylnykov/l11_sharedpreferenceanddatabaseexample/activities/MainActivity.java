package com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.activities;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.R;
import com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.ToolsAndConstants.AppSettings;
import com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.ToolsAndConstants.DbConstants;
import com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.data.Contact;
import com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.db.DataBaseHelper;
import com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.engines.ContactEngine;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashSet;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int REQUEST_CODE_TC = 1001;

    private LinearLayout conteinerLinearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        HashSet<Float> arr = new HashSet<>();

        AppSettings appSettings = new AppSettings(this);
        if (appSettings.isNeedShowTC()) {
            Intent intent = new Intent(this, TandCActivity.class);
            startActivityForResult(intent, REQUEST_CODE_TC);

            ContactEngine contactEngine = new ContactEngine(this);
            for (int i=0; i<2000; i++) {
                contactEngine.insetrItem(new Contact("Name" + i, "SName" + i,
                        "09" + i + "052465" + i*10, "str. Devichya " + i));
            }
        }

        conteinerLinearLayout = findViewById(R.id.conteinerLinearLayout);
        findViewById(R.id.addBtnMainActivity).setOnClickListener(this);
        findViewById(R.id.removeAllBtnMainActivity).setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateDataOnScreen();
    }

    private void updateDataOnScreen(){
        conteinerLinearLayout.removeAllViews();

        ContactEngine contactEngine = new ContactEngine(this);
        ArrayList<Contact> contacts = contactEngine.getAll();

        LinearLayout.LayoutParams params =
                new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT);

        int topAndBottomMargin = (int) getResources()
                .getDimension(R.dimen.top_and_bottom_margin_for_list);

        for (Contact contact : contacts) {
            TextView textView = new TextView(this);

            params.setMargins(0, topAndBottomMargin, 0, topAndBottomMargin);

            textView.setLayoutParams(params);
            textView.setText(MessageFormat.format("'{'{0} {1}'}'\n{2}\n{0} - {3}",
                    contact.getName(), contact.getSname(), contact.getPhone(), contact.getAdress()));
            textView.setTag(contact.getId());
            textView.setOnClickListener(this);
            conteinerLinearLayout.addView(textView);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_TC) {
            AppSettings appSettings = new AppSettings(this);
            if (appSettings.isNeedShowTC()) {
                Toast.makeText(this, "Вы не приняли соглашение!(((", Toast.LENGTH_LONG).show();
                finish();
            }
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getTag() != null && v.getTag() instanceof Long) {
            long id = (long) v.getTag();
            Intent intent = new Intent(this, EditActivity.class);
            intent.putExtra(EditActivity.USER_ID, id);
            startActivity(intent);
        } else {
            switch (v.getId()) {
                case R.id.addBtnMainActivity:
                    startActivity(new Intent(this, EditActivity.class));
                    break;
                case R.id.removeAllBtnMainActivity:
                    ContactEngine contactEngine = new ContactEngine(this);
                    contactEngine.removeAll();
                    break;
            }
        }

    }
}