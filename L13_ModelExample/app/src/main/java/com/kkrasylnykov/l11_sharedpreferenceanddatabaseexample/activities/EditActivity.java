package com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.activities;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.R;
import com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.ToolsAndConstants.DbConstants;
import com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.data.Contact;
import com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.db.DataBaseHelper;
import com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.engines.ContactEngine;

public class EditActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String USER_ID = "USER_ID";

    private EditText nameEditText;
    private EditText snameEditText;
    private EditText phoneEditText;
    private EditText adressEditText;

    private long userId = -1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            userId = bundle.getLong(USER_ID, -1);
        }

        nameEditText = findViewById(R.id.nameEditTextEditActivity);
        snameEditText = findViewById(R.id.snameEditTextEditActivity);
        phoneEditText = findViewById(R.id.phoneEditTextEditActivity);
        adressEditText = findViewById(R.id.adressEditTextEditActivity);

        Button addButton = findViewById(R.id.addBtnEditActivity);
        addButton.setOnClickListener(this);

        if (userId != -1) {
            ContactEngine contactEngine = new ContactEngine(this);
            Contact contact = contactEngine.getItemById(userId);

            nameEditText.setText(contact.getName());
            snameEditText.setText(contact.getSname());
            phoneEditText.setText(contact.getPhone());
            adressEditText.setText(contact.getAdress());

            addButton.setText("Update");

            Button removeButton = findViewById(R.id.removeBtnEditActivity);
            removeButton.setOnClickListener(this);
            removeButton.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClick(View v) {
        ContactEngine contactEngine = new ContactEngine(this);
        switch (v.getId()){
            case R.id.removeBtnEditActivity:
                contactEngine.removeItemById(userId);
                finish();
                break;
            case R.id.addBtnEditActivity:
                String name = nameEditText.getText().toString();
                String sname = snameEditText.getText().toString();
                String phone = phoneEditText.getText().toString();
                String adress = adressEditText.getText().toString();

                if (name != null && !name.isEmpty()) {
                    Contact contact = new Contact(userId, name, sname, phone, adress);

                    if (userId != -1) {
                        contactEngine.updateItem(contact);
                    } else {
                        contactEngine.insetrItem(contact);
                    }

                    if (userId != -1) {
                        finish();
                    } else {
                        nameEditText.setText("");
                        snameEditText.setText("");
                        phoneEditText.setText("");
                        adressEditText.setText("");

                        nameEditText.requestFocus();
                    }

                } else {
                    Toast.makeText(this, "Need field name not null or empty!", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }
}
