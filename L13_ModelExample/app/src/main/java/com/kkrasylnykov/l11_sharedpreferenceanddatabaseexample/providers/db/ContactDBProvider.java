package com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.providers.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.R;
import com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.ToolsAndConstants.DbConstants;
import com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.data.Contact;
import com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.db.DataBaseHelper;

import java.text.MessageFormat;
import java.util.ArrayList;

public class ContactDBProvider {

    private DataBaseHelper dbHelper;

    public ContactDBProvider(Context context){
        dbHelper = new DataBaseHelper(context);
    }

    public ArrayList<Contact> getAll(){
        ArrayList<Contact> result = new ArrayList<>();
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        Cursor cursor = db.query(DbConstants.TABLE_NAME, null, null, null,
                null, null, null);

        if (cursor != null) {
            int idPosition = cursor.getColumnIndex(DbConstants.FIELD_ID);
            int namePosition = cursor.getColumnIndex(DbConstants.FIELD_NAME);
            int snamePosition = cursor.getColumnIndex(DbConstants.FIELD_SNAME);
            int phonePosition = cursor.getColumnIndex(DbConstants.FIELD_PHONE);
            int adressPosition = cursor.getColumnIndex(DbConstants.FIELD_ADRESS);
            if (cursor.moveToFirst()) {
                do{
                    long id = cursor.getLong(idPosition);
                    String name = cursor.getString(namePosition);
                    String sname = cursor.getString(snamePosition);
                    String phone = cursor.getString(phonePosition);
                    String adress = cursor.getString(adressPosition);

                    Contact contact = new Contact(id, name, sname, phone, adress);
                    result.add(contact);
                } while(cursor.moveToNext());
            }
            cursor.close();
        }
        db.close();
        return result;
    }

    public Contact getItemById(long id){
        Contact contact = null;
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        String selection = DbConstants.FIELD_ID + "=?";
        String[] selectionArgs = new String[]{Long.toString(id)};

        Cursor cursor = db.query(DbConstants.TABLE_NAME, null,
                selection, selectionArgs,
                null, null, null);
        if (cursor != null) {
            int namePosition = cursor.getColumnIndex(DbConstants.FIELD_NAME);
            int snamePosition = cursor.getColumnIndex(DbConstants.FIELD_SNAME);
            int phonePosition = cursor.getColumnIndex(DbConstants.FIELD_PHONE);
            int adressPosition = cursor.getColumnIndex(DbConstants.FIELD_ADRESS);
            if (cursor.moveToFirst()) {
                String name = cursor.getString(namePosition);
                String sname = cursor.getString(snamePosition);
                String phone = cursor.getString(phonePosition);
                String adress = cursor.getString(adressPosition);

                contact = new Contact(id, name, sname, phone, adress);
            }

            cursor.close();
        }
        db.close();
        return contact;
    }

    public void insetrItem(Contact contact){
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(DbConstants.FIELD_NAME, contact.getName());
        contentValues.put(DbConstants.FIELD_SNAME, contact.getSname());
        contentValues.put(DbConstants.FIELD_PHONE, contact.getPhone());
        contentValues.put(DbConstants.FIELD_ADRESS, contact.getAdress());
        db.insert(DbConstants.TABLE_NAME, null, contentValues);
        db.close();
    }

    public void updateItem(Contact contact){
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        String selection = DbConstants.FIELD_ID + "=?";
        String[] selectionArgs = new String[]{Long.toString(contact.getId())};

        ContentValues contentValues = new ContentValues();
        contentValues.put(DbConstants.FIELD_NAME, contact.getName());
        contentValues.put(DbConstants.FIELD_SNAME, contact.getSname());
        contentValues.put(DbConstants.FIELD_PHONE, contact.getPhone());
        contentValues.put(DbConstants.FIELD_ADRESS, contact.getAdress());

        db.update(DbConstants.TABLE_NAME, contentValues, selection, selectionArgs);

        db.close();
    }

    public void removeItemById(long id){
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        String selection = DbConstants.FIELD_ID + "=?";
        String[] selectionArgs = new String[]{Long.toString(id)};
        db.delete(DbConstants.TABLE_NAME, selection, selectionArgs);
        db.close();
    }

    public void removeAll(){
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        db.delete(DbConstants.TABLE_NAME, null, null);
        db.close();
    }
}
