package com.kkrasylnykov.l08_towfragmentexample.activities;

import android.content.res.Configuration;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.kkrasylnykov.l08_towfragmentexample.R;
import com.kkrasylnykov.l08_towfragmentexample.fragments.ListFragment;
import com.kkrasylnykov.l08_towfragmentexample.fragments.ViewerFragment;

public class MainActivity extends AppCompatActivity {

    ListFragment listFragment;
    ViewerFragment viewerFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listFragment = new ListFragment();
        viewerFragment = new ViewerFragment();

        FragmentTransaction ft = getSupportFragmentManager()
                .beginTransaction();
        ft.add(R.id.mainFrameLayout, listFragment);
        if (getResources().getConfiguration().orientation
                == Configuration.ORIENTATION_LANDSCAPE) {
            ft.add(R.id.secondFrameLayout, viewerFragment);
        }
        ft.commit();
    }

    public void showData(String s) {
        if (getResources().getConfiguration().orientation
                == Configuration.ORIENTATION_PORTRAIT) {
            FragmentTransaction ft = getSupportFragmentManager()
                    .beginTransaction();
            ft.add(R.id.mainFrameLayout, viewerFragment);
            ft.addToBackStack("mainFrameLayout");
            ft.commit();
        }
        viewerFragment.setData(s);
    }
}
