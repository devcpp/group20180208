package com.kkrasylnykov.l08_towfragmentexample.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.kkrasylnykov.l08_towfragmentexample.R;
import com.kkrasylnykov.l08_towfragmentexample.activities.MainActivity;

import java.security.acl.Group;

public class ListFragment extends Fragment
        implements View.OnClickListener {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_list,
                container, false);
        for (int i=0; i < rootView.getChildCount(); i++) {
            View view = rootView.getChildAt(i);
            if (view instanceof Button) {
                Button button = (Button) view;
                button.setOnClickListener(this);
            }
        }

        return rootView;
    }

    @Override
    public void onClick(View v) {
        if (v instanceof Button) {
            ((MainActivity)getActivity()).showData(((Button)v).getText().toString());
        }
    }
}
