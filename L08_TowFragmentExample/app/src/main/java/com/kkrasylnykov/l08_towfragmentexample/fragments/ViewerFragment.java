package com.kkrasylnykov.l08_towfragmentexample.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.kkrasylnykov.l08_towfragmentexample.R;
import com.kkrasylnykov.l08_towfragmentexample.activities.MainActivity;

public class ViewerFragment extends Fragment {

    TextView textView;
    String text;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_viewer,
                container, false);
        textView = rootView.findViewById(R.id.textViewer);
        if(text!=null) {
            textView.setText(text);
        }

        return rootView;
    }

    public void setData(String str){
        text = str;
        if (textView != null) {
            textView.setText(text);
        }
    }
}
