package com.kkrasylnykov.l21_networkexample;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.kkrasylnykov.l21_networkexample.data.Article;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ArticlesAdapter extends RecyclerView.Adapter<ArticlesAdapter.ArcitcleVH> {

    private ArrayList<Article> data;

    @NonNull
    @Override
    public ArcitcleVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ArcitcleVH(LayoutInflater.from(
                parent.getContext())
                .inflate(R.layout.item_article, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ArcitcleVH holder, int position) {
        Article article = data.get(position);

        holder.titleTextView.setText(article.getTitle());
        holder.dateTextView.setText(article.getPublishedAt());

        Picasso.get().load(article.getUrlToImage())
                .placeholder(R.mipmap.ic_launcher_round)
                .error(R.mipmap.ic_launcher)
                .into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return data == null ? 0 : data.size();
    }

    public void setData(ArrayList<Article> data){
        if (data == null || data.size() == 0) {
            return;
        }

        if (this.data == null) {
            this.data = new ArrayList<>();
        } else {
            this.data.clear();
        }

        this.data.addAll(data);
        notifyDataSetChanged();
    }

    public class ArcitcleVH extends RecyclerView.ViewHolder{

        TextView titleTextView;
        TextView dateTextView;
        ImageView imageView;

        public ArcitcleVH(View itemView) {
            super(itemView);
            titleTextView = itemView.findViewById(R.id.titleTextView);
            dateTextView = itemView.findViewById(R.id.dateTextView);
            imageView = itemView.findViewById(R.id.imageView);
        }
    }
}
