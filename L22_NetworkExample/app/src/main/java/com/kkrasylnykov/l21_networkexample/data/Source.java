package com.kkrasylnykov.l21_networkexample.data;

import org.json.JSONException;
import org.json.JSONObject;

public class Source {
    private String id;
    private String name;

    public Source(JSONObject object) throws JSONException {
        id = object.getString("id");
        name = object.getString("name");
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
