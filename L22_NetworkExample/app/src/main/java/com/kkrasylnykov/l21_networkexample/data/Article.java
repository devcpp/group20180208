package com.kkrasylnykov.l21_networkexample.data;

import org.json.JSONException;
import org.json.JSONObject;

public class Article {

    private Source source;
    private String author;
    private String title;
    private String description;
    private String url;
    private String urlToImage;
    private String publishedAt;

    public Article(JSONObject object) throws JSONException{
        source = new Source(object.getJSONObject("source"));
        author = object.getString("author");
        title = object.getString("title");
        description = object.getString("description");
        url = object.getString("url");
        urlToImage = object.getString("urlToImage");
        publishedAt = object.getString("publishedAt");
    }

    public Article(Article object) {
        source = object.source;
        author = object.author;
        title = object.title;
        description = object.description;
        url = object.url;
        urlToImage = object.urlToImage;
        publishedAt = object.publishedAt;
    }

    public Source getSource() {
        return source;
    }

    public String getAuthor() {
        return author;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getUrl() {
        return url;
    }

    public String getUrlToImage() {
        return urlToImage;
    }

    public String getPublishedAt() {
        return publishedAt;
    }
}
