package com.kkrasylnykov.l09_customviewexample.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kkrasylnykov.l09_customviewexample.R;

public class TextAndImageView extends LinearLayout {
    private TextView textView;
    private ImageView imageView;

    public TextAndImageView(Context context) {
        this(context, null);
    }

    public TextAndImageView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public TextAndImageView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    private void init(AttributeSet attrs) {
        LayoutInflater.from(getContext()).inflate(R.layout.view_text_and_image, this,
                true);
        textView = findViewById(R.id.textCustomView);
        imageView = findViewById(R.id.imageCustomView);

        if (attrs != null) {
            TypedArray ta = getContext().obtainStyledAttributes(attrs, R.styleable.TextAndImageView);
            String text = ta.getString(R.styleable.TextAndImageView_text);
//            int textId = ta.getInt(R.styleable.TextAndImageView_text, 0);

            int imageId = ta.getResourceId(R.styleable.TextAndImageView_image, R.mipmap.ic_launcher);
            ta.recycle();

//            if (textId != 0) {
//                textView.setText(textId);
//            } else
            if (text != null) {
                textView.setText(text);
            }

            imageView.setImageResource(imageId);
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        int imageHeight = imageView.getMeasuredHeight();

        int n = 0;
        while (imageHeight < textView.getMeasuredHeight()) {
            textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, textView.getTextSize() - 1);
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
            Log.d("devcpp", Integer.toString(++n));
        }
    }
}
