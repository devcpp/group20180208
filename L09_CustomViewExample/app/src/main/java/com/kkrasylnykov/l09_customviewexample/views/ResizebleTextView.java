package com.kkrasylnykov.l09_customviewexample.views;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.widget.TextView;

public class ResizebleTextView extends android.support.v7.widget.AppCompatTextView {

    public ResizebleTextView(Context context) {
        this(context, null);
    }

    public ResizebleTextView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, android.R.attr.textViewStyle);
    }

    public ResizebleTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int unspecMeasureSpec = MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED);
        super.onMeasure(unspecMeasureSpec, heightMeasureSpec);

        int maxWidth = MeasureSpec.getSize(widthMeasureSpec);

        if (getMeasuredWidth() > maxWidth) {
            float delta = ((float)maxWidth)/getMeasuredWidth();

            float textSize = getTextSize() * delta;

//            do {
                setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
                super.onMeasure(unspecMeasureSpec, heightMeasureSpec);

//                textSize -= 10;
//            } while (getMeasuredWidth() > maxWidth);
        }
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
    }
}
