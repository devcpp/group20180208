package com.kkrasylnykov.l09_customviewexample.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.kkrasylnykov.l09_customviewexample.R;

public class MainActivity extends AppCompatActivity
        implements View.OnClickListener {

    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView = findViewById(R.id.textViewMainActivity);

        textView.setOnClickListener(this);

        Log.d("devcpp","onCreate -> " + textView.getWidth());
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("devcpp","onStart -> " + textView.getWidth());
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("devcpp","onResume -> " + textView.getWidth());
    }

    @Override
    public void onClick(View v) {
        Log.d("devcpp","onClick -> " + textView.getWidth());
    }
}
