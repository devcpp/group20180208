package com.kkrasylnykov.l28_intentfiltersexample;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity
        implements View.OnClickListener {

    private EditText editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editText = findViewById(R.id.editText);

        findViewById(R.id.view).setOnClickListener(this);
        findViewById(R.id.phone).setOnClickListener(this);
        findViewById(R.id.email).setOnClickListener(this);
        findViewById(R.id.geo).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.view:
                try {
                    String url = "http://" + editText.getText().toString();
                    Intent i = new Intent();
                    i.setAction(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(url));
                    startActivity(i);
                } catch (ActivityNotFoundException e) {
                    e.printStackTrace();
                    Toast.makeText(this, "Application not foud!", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.phone:
                try {
                    String url = "tel:" + editText.getText().toString();
                    Intent i = new Intent();
//                    i.setAction(Intent.ACTION_CALL);
                    i.setAction(Intent.ACTION_DIAL);
                    i.setData(Uri.parse(url));
                    startActivity(i);
                } catch (ActivityNotFoundException e) {
                    e.printStackTrace();
                    Toast.makeText(this, "Application not foud!", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.email:
                try {
                    String text = editText.getText().toString();
                    Intent i = new Intent();
//                    i.setAction(Intent.ACTION_CALL);
                    i.setAction(Intent.ACTION_SEND);
                    i.setType("text/html");
//                    i.setType("text/plain");
                    i.putExtra(Intent.EXTRA_TEXT, text);
                    startActivity(i);
                } catch (ActivityNotFoundException e) {
                    e.printStackTrace();
                    Toast.makeText(this, "Application not foud!", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.geo:
                break;
        }
    }
}
