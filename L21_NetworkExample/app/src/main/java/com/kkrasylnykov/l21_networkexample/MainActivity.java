package com.kkrasylnykov.l21_networkexample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {
    private static  final String BASE_URL = "http://newsapi.org";

    private static final String PATH_TOP_HEADLINES = "/v2/top-headlines";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        HashMap<String, String> params = new HashMap<>();
        params.put("country", "ua");
        params.put("category", "business");
        params.put("apiKey", "42cf363df48144809ba9a3eb87e8b24c");

        String paramsStr = "";

        for(Map.Entry<String, String> entry : params.entrySet()) {
            if (!paramsStr.isEmpty()) {
                paramsStr = paramsStr + "&";
            }

            paramsStr = paramsStr + entry.getKey() + "=" + entry.getValue();
        }

        final String strURL = BASE_URL + PATH_TOP_HEADLINES + "?" + paramsStr;

        Log.d("devcpp", "strURL -> " + strURL);

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    URL url = new URL(strURL);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();

                    connection.setReadTimeout(10000);
                    connection.setConnectTimeout(10000);

                    connection.setRequestMethod("GET");

                    connection.setRequestProperty("Content-Type", "application/json");
                    connection.setRequestProperty("charset", "utf-8");

                    connection.connect();

                    int responseCode = connection.getResponseCode();

                    if (responseCode == 200) {
                        InputStream is = connection.getInputStream();
                        String strResponse = "";
                        if (is!=null){
                            BufferedReader reader = null;
                            reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));

                            if (reader!=null){
                                StringBuilder stringBuilder = new StringBuilder();
                                while (true){
                                    String tempStr = reader.readLine();
                                    if(tempStr != null){
                                        stringBuilder.append(tempStr);
//                                strResponse += tempStr; - NOT TRUE!!!!!
                                    } else {
                                        strResponse = stringBuilder.toString();
                                        break;
                                    }
                                }
                                reader.close();
                            }
                            is.close();
                        }

                        Log.d("devcpp", "response -> " + responseCode);
                        Log.d("devcpp", "strResponse -> " + strResponse);
                    }


                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();

    }
}
