package com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.R;
import com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.data.Contact;
import com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.data.Phone;
import com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.engines.ContactEngine;

import java.util.ArrayList;

public class EditActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String USER_ID = "USER_ID";
    public static final int REMOVE_USER = 1080;
    public static final int UPDATE_USER = 1081;

    private EditText nameEditText;
    private EditText snameEditText;
    private EditText adressEditText;

    private LinearLayout phoneContainer;

    private long userId = -1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            userId = bundle.getLong(USER_ID, -1);
        }

        nameEditText = findViewById(R.id.nameEditTextEditActivity);
        snameEditText = findViewById(R.id.snameEditTextEditActivity);
//        phoneEditText = findViewById(R.id.phoneEditTextEditActivity);
        phoneContainer = findViewById(R.id.phoneContainer);
        adressEditText = findViewById(R.id.adressEditTextEditActivity);

        findViewById(R.id.addPhoneField).setOnClickListener(this);

        Button addButton = findViewById(R.id.addBtnEditActivity);
        addButton.setOnClickListener(this);

        if (userId != -1) {
            ContactEngine contactEngine = new ContactEngine(this);
            Contact contact = contactEngine.getItemById(userId);

            nameEditText.setText(contact.getName());
            snameEditText.setText(contact.getSname());
            //TODO phoneEditText.setText(contact.getPhone());
            adressEditText.setText(contact.getAdress());

            addButton.setText("Update");

            Button removeButton = findViewById(R.id.removeBtnEditActivity);
            removeButton.setOnClickListener(this);
            removeButton.setVisibility(View.VISIBLE);
        }

        addPhoneFieldOnContainer();
    }

    private void addPhoneFieldOnContainer(){
        EditText editText = new EditText(this);
        LinearLayout.LayoutParams params = new LinearLayout
                .LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        editText.setLayoutParams(params);
        editText.setHint("Enter phone");
        editText.setInputType(InputType.TYPE_CLASS_PHONE);

        phoneContainer.addView(editText);

    }

    @Override
    public void onClick(View v) {
        ContactEngine contactEngine = new ContactEngine(this);
        switch (v.getId()){
            case R.id.addPhoneField:
                addPhoneFieldOnContainer();
                break;
            case R.id.removeBtnEditActivity:
                contactEngine.removeItemById(userId);
                Intent intent = new Intent();
                intent.putExtra(USER_ID, userId);
                setResult(REMOVE_USER, intent);
                finish();
                break;
            case R.id.addBtnEditActivity:
                String name = nameEditText.getText().toString();
                String sname = snameEditText.getText().toString();
                String adress = adressEditText.getText().toString();
                ArrayList<Phone> phones = new ArrayList<>();
                for (int i = 0; i < phoneContainer.getChildCount(); i++) {
                    View view = phoneContainer.getChildAt(i);
                    if (view instanceof EditText) {
                        String text = ((EditText)view).getText().toString();
                        if (text != null && !text.isEmpty()) {
                            phones.add(new Phone(text));
                        }
                    }
                }

                if (name != null && !name.isEmpty()) {
                    Contact contact = new Contact(userId, -1, name, sname, phones, adress);

                    if (userId != -1) {
                        contactEngine.updateItem(contact);
                    } else {
                        final ProgressDialog progressDialog = new ProgressDialog(this);
                        progressDialog.setCancelable(false);
                        progressDialog.show();
                        contactEngine.insertItem(contact, new ContactEngine.SendDataOnServerListener() {
                            @Override
                            public void onComplite() {
                                progressDialog.dismiss();
                            }
                        });
                    }

                    if (userId != -1) {
                        Intent intentInsert = new Intent();
                        intentInsert.putExtra(USER_ID, userId);
                        setResult(UPDATE_USER, intentInsert);
                        finish();
                    } else {
                        nameEditText.setText("");
                        snameEditText.setText("");
                        phoneContainer.removeAllViews();
                        addPhoneFieldOnContainer();
                        adressEditText.setText("");

                        nameEditText.requestFocus();
                    }

                } else {
                    Toast.makeText(this, "Need field name not null or empty!", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }
}
