package com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.ToolsAndConstants.DbConstants;
import com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.data.Contact;

import java.util.ArrayList;

public class DataBaseHelper extends SQLiteOpenHelper {
    public DataBaseHelper(Context context) {
        super(context, DbConstants.DB_NAME,
                null, DbConstants.DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + DbConstants.DB_V2.TABLE_USER_INFO.TABLE_NAME + " (" +
                DbConstants.DB_V2.TABLE_USER_INFO.FIELDS.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                DbConstants.DB_V2.TABLE_USER_INFO.FIELDS.SERVER_ID + " INTEGER, " +
                DbConstants.DB_V2.TABLE_USER_INFO.FIELDS.NAME + " TEXT NOT NULL, " +
                DbConstants.DB_V2.TABLE_USER_INFO.FIELDS.SNAME + " TEXT, " +
                DbConstants.DB_V2.TABLE_USER_INFO.FIELDS.ADRESS + " TEXT);");

        db.execSQL("CREATE TABLE " + DbConstants.DB_V2.TABLE_PHONES.TABLE_NAME + " (" +
                DbConstants.DB_V2.TABLE_PHONES.FIELDS.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                DbConstants.DB_V2.TABLE_PHONES.FIELDS.USER_ID + " INTEGER, " +
                DbConstants.DB_V2.TABLE_PHONES.FIELDS.PHONE + " TEXT);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onCreate(db);

        if (oldVersion == 1) {
            updateToV2(db);
            oldVersion = 2;
        }
    }

    private void updateToV2(SQLiteDatabase db) {
        Cursor cursor = db.query(DbConstants.DB_V1.TABLE_NAME, null, null, null,
                null, null, null);

        if (cursor != null) {
            int namePosition = cursor.getColumnIndex(DbConstants.DB_V1.FIELD_NAME);
            int snamePosition = cursor.getColumnIndex(DbConstants.DB_V1.FIELD_SNAME);
            int phonePosition = cursor.getColumnIndex(DbConstants.DB_V1.FIELD_PHONE);
            int adressPosition = cursor.getColumnIndex(DbConstants.DB_V1.FIELD_ADRESS);

            if (cursor.moveToFirst()) {
                do{
                    String name = cursor.getString(namePosition);
                    String sname = cursor.getString(snamePosition);
                    String phone = cursor.getString(phonePosition);
                    String adress = cursor.getString(adressPosition);

                    ContentValues userInfo = new ContentValues();
                    userInfo.put(DbConstants.DB_V2.TABLE_USER_INFO.FIELDS.NAME, name);
                    userInfo.put(DbConstants.DB_V2.TABLE_USER_INFO.FIELDS.SNAME, sname);
                    userInfo.put(DbConstants.DB_V2.TABLE_USER_INFO.FIELDS.ADRESS, adress);

                    long userId = db.insert(DbConstants.DB_V2.TABLE_USER_INFO.TABLE_NAME,
                            null, userInfo);

                    ContentValues phoneData = new ContentValues();
                    phoneData.put(DbConstants.DB_V2.TABLE_PHONES.FIELDS.USER_ID, userId);
                    phoneData.put(DbConstants.DB_V2.TABLE_PHONES.FIELDS.PHONE, phone);

                    db.insert(DbConstants.DB_V2.TABLE_PHONES.TABLE_NAME,
                            null, phoneData);

                } while(cursor.moveToNext());
            }
            cursor.close();
        }


        db.execSQL("DROP TABLE " + DbConstants.DB_V1.TABLE_NAME);
    }
}
