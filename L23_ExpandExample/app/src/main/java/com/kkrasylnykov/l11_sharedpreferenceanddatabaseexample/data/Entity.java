package com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.data;

import android.content.ContentValues;

public abstract class Entity {

    public abstract ContentValues getContentValues();
}
