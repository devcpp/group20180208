package com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.engines;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.data.Contact;
import com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.data.Phone;
import com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.providers.db.ContactDBProvider;
import com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.providers.network.ContactNetworkProvider;

import java.util.ArrayList;

public class ContactEngine extends BaseEngine {

    public ContactEngine(Context context) {
        super(context);
    }

    public void getAll(final GetContactCallback callback){
        ContactDBProvider dbProvider = new ContactDBProvider(getContext());
        ArrayList<Contact> data = dbProvider.getAll();
        if (data == null || data.size() == 0) {
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    ContactNetworkProvider networkProvider = new ContactNetworkProvider();
                    networkProvider.getAll(new GetContactCallback() {
                        @Override
                        public void onResult(final ArrayList<Contact> data) {
                            for (Contact contact : data) {
                                insertItem(contact, null);
                            }
//                            insetrItems(data);
                            Handler handler = new Handler(Looper.getMainLooper());
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    callback.onResult(data);
                                }
                            });

                        }
                    });
                }
            });
            thread.start();
        } else {
            if (data != null) {
                PhoneEngine phoneEngine = new PhoneEngine(getContext());
                for (Contact contact : data){
                    contact.setPhones(phoneEngine.getPhonesByUserId(contact.getId()));
                }
            }
            callback.onResult(data);
        }
    }

    public Contact getItemById(long id){
        ContactDBProvider dbProvider = new ContactDBProvider(getContext());
        return dbProvider.getItemById(id);
    }

    public void insertItem(final Contact contact, final SendDataOnServerListener callback){
        final Runnable saveDataOnDb = new Runnable() {
            @Override
            public void run() {
                Log.d("devcpp", "contact.getServerId -> " + contact.getServerId());

                ContactDBProvider dbProvider = new ContactDBProvider(getContext());
                long idUser = dbProvider.insetrItem(contact);

                PhoneEngine phoneEngine = new PhoneEngine(getContext());
                if (contact.getPhones() != null) {
                    for (Phone phone : contact.getPhones()) {
                        phone.setUserId(idUser);
                        phoneEngine.insetrItem(phone);
                    }
                }
                if (callback != null) {
                    Handler handler = new Handler(Looper.getMainLooper());
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onComplite();
                        }
                    });
                }

            }
        };

        if (contact.getServerId() == -1) {
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    ContactNetworkProvider networkProvider = new ContactNetworkProvider();
                    networkProvider.insertItem(contact);

                    saveDataOnDb.run();
                }
            });
            thread.start();
        } else {
            saveDataOnDb.run();
        }

    }

    public void insetrItems(ArrayList<Contact> contacts){
        ContactDBProvider dbProvider = new ContactDBProvider(getContext());
        dbProvider.insetrItems(contacts);
    }

    public void updateItem(Contact contact){
        ContactDBProvider dbProvider = new ContactDBProvider(getContext());
        dbProvider.updateItem(contact);
    }

    public void removeItem(Contact contact){
        removeItemById(contact.getId());
    }

    public void removeItemById(long id){
        ContactDBProvider dbProvider = new ContactDBProvider(getContext());
        dbProvider.removeItemById(id);
    }

    public void removeAll(){
        ContactDBProvider dbProvider = new ContactDBProvider(getContext());
        dbProvider.removeAll();
    }

    public interface GetContactCallback{
        void onResult(ArrayList<Contact> data);
    }

    public interface SendDataOnServerListener {
        void onComplite();
    }
}
