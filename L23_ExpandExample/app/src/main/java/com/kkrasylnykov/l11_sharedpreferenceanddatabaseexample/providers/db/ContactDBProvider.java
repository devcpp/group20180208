package com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.providers.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.R;
import com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.ToolsAndConstants.DbConstants;
import com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.data.Contact;
import com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.data.Phone;
import com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.db.DataBaseHelper;

import java.text.MessageFormat;
import java.util.ArrayList;

public class ContactDBProvider extends BaseDBProvider {


    public ContactDBProvider(Context context) {
        super(context, DbConstants.DB_V2.TABLE_USER_INFO.TABLE_NAME);
    }

    public ArrayList<Contact> getAll(){
        ArrayList<Contact> result = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = db.query(getTableName(), null, null, null,
                null, null, null);

        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do{
                    Contact contact = new Contact(cursor);
                    result.add(contact);
                } while(cursor.moveToNext());
            }
            cursor.close();
        }
        db.close();
        return result;
    }

    public Contact getItemById(long id){
        Contact contact = null;
        SQLiteDatabase db = getReadableDatabase();

        String selection = DbConstants.DB_V2.TABLE_USER_INFO.FIELDS.ID + "=?";
        String[] selectionArgs = new String[]{Long.toString(id)};

        Cursor cursor = db.query(getTableName(), null,
                selection, selectionArgs,
                null, null, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                contact = new Contact(cursor);
            }

            cursor.close();
        }
        db.close();
        return contact;
    }

    public long insetrItem(Contact contact){
        SQLiteDatabase db = getWritableDatabase();
        long idUser = db.insert(getTableName(),
                null,
                contact.getContentValues());
        db.close();
        return idUser;
    }

    public void insetrItems(ArrayList<Contact> contacts){
        SQLiteDatabase db = getWritableDatabase();
        int count = 0;
        for (Contact contact : contacts) {
            if (count == 0) {
                Log.d("devcpp","beginTransaction");
                db.beginTransaction();
            }
            count++;
            db.insert(getTableName(), null, contact.getContentValues());
            if (count == 10000) {
                db.setTransactionSuccessful();
                db.endTransaction();
                Log.d("devcpp","endTransaction");
                count = 0;
            }
        }
        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();
    }

    public void updateItem(Contact contact){
        SQLiteDatabase db = getWritableDatabase();

        String selection = DbConstants.DB_V2.TABLE_USER_INFO.FIELDS.ID + "=?";
        String[] selectionArgs = new String[]{Long.toString(contact.getId())};

        db.update(getTableName(), contact.getContentValues(), selection, selectionArgs);

        db.close();
    }

    public void removeItemById(long id){
        SQLiteDatabase db = getWritableDatabase();

        String selection = DbConstants.DB_V2.TABLE_USER_INFO.FIELDS.ID + "=?";
        String[] selectionArgs = new String[]{Long.toString(id)};
        db.delete(getTableName(), selection, selectionArgs);
        db.close();
    }

    public void removeAll(){
        SQLiteDatabase db = getWritableDatabase();
        db.delete(getTableName(), null, null);
        db.close();
    }
}
