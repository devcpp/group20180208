package com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.providers.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.ToolsAndConstants.DbConstants;
import com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.data.Contact;
import com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.data.Phone;

import java.util.ArrayList;

public class PhoneDBProvider extends BaseDBProvider {
    public PhoneDBProvider(Context context) {
        super(context, DbConstants.DB_V2.TABLE_PHONES.TABLE_NAME);
    }

    public ArrayList<Phone> getPhonesByUserId(long userId) {
        ArrayList<Phone> result = new ArrayList<>();
        String selection = DbConstants.DB_V2.TABLE_PHONES.FIELDS.USER_ID + "=?";
        String[] args = new String[]{Long.toString(userId)};
        Cursor cursor = getReadableDatabase().query(getTableName(), null, selection, args,
                null, null, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    result.add(new Phone(cursor));
                } while (cursor.moveToNext());
            }
            cursor.close();
        }
        return result;
    }

    public long insetrItem(Phone phone){
        SQLiteDatabase db = getWritableDatabase();
        long idPhone = db.insert(getTableName(),
                null,
                phone.getContentValues());
        db.close();
        return idPhone;
    }
}
