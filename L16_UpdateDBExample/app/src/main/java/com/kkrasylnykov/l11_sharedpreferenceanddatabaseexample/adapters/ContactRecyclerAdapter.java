package com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.adapters;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.R;
import com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.data.Contact;
import com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.data.Phone;

import java.text.MessageFormat;
import java.util.ArrayList;

public class ContactRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private int CONTACT_ITEM = 0;
    private int HEADER_ITEM = 1;

    private ArrayList<Contact> data;
    private OnContactClickListener contactClickListener;

    public ContactRecyclerAdapter(ArrayList<Contact> data) {
        this.data = data;
    }

    public void setOnContactClickListener(OnContactClickListener contactClickListener){
        this.contactClickListener = contactClickListener;
    }

    @Override
    public int getItemViewType(int position) {
        return position == 0 ? HEADER_ITEM : CONTACT_ITEM;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder holder = null;

        if (viewType == HEADER_ITEM) {
            holder = new HeaderViewHolder(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_header, parent, false));
        } else if (viewType == CONTACT_ITEM) {
            holder = new ContactViewHolder(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_contact, parent, false));
        }

        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) == HEADER_ITEM) {
            HeaderViewHolder headerViewHolder = (HeaderViewHolder) holder;
            headerViewHolder.count.setText(Integer.toString(data.size()));
        } else if (getItemViewType(position) == CONTACT_ITEM) {
            ContactViewHolder contactViewHolder = (ContactViewHolder) holder;
            Contact contact = data.get(getRealPosition(position));
            contactViewHolder.name.setText(MessageFormat.format("{0} {1}",
                    contact.getName(), contact.getSname()));
            ArrayList<Phone> phones = contact.getPhones();
            StringBuilder stringBuilder = new StringBuilder();
            if (phones != null) {
                for (Phone phone : phones) {
                    stringBuilder.append(phone);
                    stringBuilder.append("\n");
                }

            }
            contactViewHolder.phone.setText(stringBuilder.toString());
        }

    }

    @Override
    public int getItemCount() {
        return this.data.size() + 1;
    }

    public void moveData(){
        Contact contact = data.get(data.size() - 1);
        data.remove(data.size() - 1);
        data.add(data.size() - 3, contact);

        notifyItemMoved(1, 3);
    }

    public void updateData(Contact contact) {
        if (contact == null) {
            contact = data.get(data.size() - 4);
            contact.setName("updateData");
        }

       int position = data.indexOf(contact);

       if (position > -1) {
           int updatePosition = getRealPosition(position);

           data.set(position, contact);
           notifyItemChanged(updatePosition);
       }
    }

    public void removeItem(long userId) {
        if (userId == -1) {
            userId = data.get(data.size() - 5).getId();
        }
        int position = 0;
        for (Contact contact : data) {
            if (contact.getId() == userId) {
                break;
            }
            position++;
        }

        Log.d("devcpp", "position -> " + position);

        int updatePosition = getRealPosition(position);

        data.remove(position);

        notifyItemRemoved(updatePosition);
    }

    public void insertItem(Contact contact) {
        data.add(contact);
        notifyItemInserted(1);
    }

    public int getRealPosition(int position) {
        int result = data.size() - position;
        return result;
    }

    public class HeaderViewHolder extends RecyclerView.ViewHolder {

        TextView count;

        public HeaderViewHolder(View itemView) {
            super(itemView);
            count = itemView.findViewById(R.id.countItem);
        }
    }

    public class ContactViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView name;
        TextView phone;

        public ContactViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.nameContactItem);
            phone = itemView.findViewById(R.id.phoneContactItem);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (contactClickListener != null) {
                long id = data.get(getRealPosition(getAdapterPosition())).getId();
                contactClickListener.onContactClick(id);
            }
        }
    }

    public interface OnContactClickListener {
        void onContactClick(long id);
    }
}