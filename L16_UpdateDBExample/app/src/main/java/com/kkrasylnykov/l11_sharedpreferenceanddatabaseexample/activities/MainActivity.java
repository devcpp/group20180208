package com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.activities;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.R;
import com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.ToolsAndConstants.AppSettings;
import com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.ToolsAndConstants.DbConstants;
import com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.adapters.ContactRecyclerAdapter;
import com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.adapters.ContactsAdapter;
import com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.data.Contact;
import com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.data.Phone;
import com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.db.DataBaseHelper;
import com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.engines.ContactEngine;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashSet;

public class MainActivity extends AppCompatActivity implements View.OnClickListener,
        ContactRecyclerAdapter.OnContactClickListener {

    private static final int REQUEST_CODE_TC = 1001;
    private static final int REQUEST_CODE_EDIT = 1002;

    private RecyclerView recyclerView;
    private ContactRecyclerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        HashSet<Float> arr = new HashSet<>();

        AppSettings appSettings = new AppSettings(this);
        if (appSettings.isNeedShowTC()) {
            Intent intent = new Intent(this, TandCActivity.class);
            startActivityForResult(intent, REQUEST_CODE_TC);

            ContactEngine contactEngine = new ContactEngine(this);
            ArrayList<Contact> data = new ArrayList<>();
            Log.d("devcpp","data -> ");
            for (int i=0; i<100; i++) {
                data.add(new Contact("Name" + i, "SName" + i,
                        new ArrayList<Phone>(), "str. Devichya " + i));
            }
            Log.d("devcpp","insetrItems -> " + data.size());
            contactEngine.insetrItems(data);

        }

        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        findViewById(R.id.addBtnMainActivity).setOnClickListener(this);
        findViewById(R.id.removeAllBtnMainActivity).setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateDataOnScreen();
    }

    private void updateDataOnScreen(){
        ContactEngine contactEngine = new ContactEngine(this);
        ArrayList<Contact> contacts = contactEngine.getAll();

        if (adapter == null) {
            adapter = new ContactRecyclerAdapter(contacts);
            adapter.setOnContactClickListener(this);
            recyclerView.setAdapter(adapter);
        }
//        else {
//            adapter.updateData(contacts);
//        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_TC) {
            AppSettings appSettings = new AppSettings(this);
            if (appSettings.isNeedShowTC()) {
                Toast.makeText(this, "Вы не приняли соглашение!(((", Toast.LENGTH_LONG).show();
                finish();
            }
        } else if (requestCode == REQUEST_CODE_EDIT) {
            long userID = data.getExtras().getLong(EditActivity.USER_ID, -1);
            if (userID == -1) {
                return;
            }

            if (resultCode == EditActivity.REMOVE_USER) {
                adapter.removeItem(userID);
            } else if (resultCode == EditActivity.UPDATE_USER) {

            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.addBtnMainActivity:
                startActivity(new Intent(this, EditActivity.class));
                break;
            case R.id.removeAllBtnMainActivity:
                ContactEngine contactEngine = new ContactEngine(this);
                contactEngine.removeAll();
                break;
        }

    }

    @Override
    public void onContactClick(long id) {
        Log.d("devcpp", "onItemClick -> " + id);
        Intent intent = new Intent(this, EditActivity.class);
        intent.putExtra(EditActivity.USER_ID, id);
        startActivityForResult(intent, REQUEST_CODE_EDIT);
    }
}