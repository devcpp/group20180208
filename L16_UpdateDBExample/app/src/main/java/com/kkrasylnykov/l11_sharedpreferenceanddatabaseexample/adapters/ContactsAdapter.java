package com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.adapters;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.R;
import com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.data.Contact;

import java.util.ArrayList;

public class ContactsAdapter extends BaseAdapter {

    private ArrayList<Contact> data;

    public ContactsAdapter(ArrayList<Contact> data) {
        this.data = data;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return ((Contact)getItem(position)).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Log.d("devcpp", position + " -> " + convertView);
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_contact, parent, false);

            viewHolder = new ViewHolder();
            viewHolder.name = convertView.findViewById(R.id.nameContactItem);
            viewHolder.phone = convertView.findViewById(R.id.phoneContactItem);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Contact contact = (Contact) getItem(position);

        viewHolder.name.setText(contact.getName() + " " + contact.getSname());
//        viewHolder.phone.setText(contact.getPhone());
        return convertView;
    }

    public void updateData(ArrayList<Contact> data) {
        this.data = data;
        notifyDataSetChanged();
    }



    public class ViewHolder{
        TextView name;
        TextView phone;
    }
}