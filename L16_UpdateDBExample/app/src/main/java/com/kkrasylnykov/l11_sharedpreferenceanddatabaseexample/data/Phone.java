package com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.data;

public class Phone {
    private long id;
    private long userId;
    private String Phone;

    public Phone(String phone) {
        this(-1, -1, phone);
    }

    public Phone(long userId, String phone) {
        this(-1, userId, phone);
    }

    public Phone(long id, long userId, String phone) {
        this.id = id;
        this.userId = userId;
        Phone = phone;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }
}
