package com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.providers.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.db.DataBaseHelper;

public abstract class BaseDBProvider {

    private DataBaseHelper dbHelper;
    private String tableName;

    public BaseDBProvider(Context context, String tableName){
        dbHelper = new DataBaseHelper(context);
        this.tableName = tableName;
    }

    protected SQLiteDatabase getReadableDatabase(){
        return dbHelper.getReadableDatabase();
    }

    protected SQLiteDatabase getWritableDatabase(){
        return dbHelper.getWritableDatabase();
    }

    protected String getTableName() {
        return tableName;
    }
}
