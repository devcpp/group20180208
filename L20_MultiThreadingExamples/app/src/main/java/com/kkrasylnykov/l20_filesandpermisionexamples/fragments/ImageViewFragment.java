package com.kkrasylnykov.l20_filesandpermisionexamples.fragments;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.kkrasylnykov.l20_filesandpermisionexamples.R;

import java.io.File;

public class ImageViewFragment extends Fragment {

    public static final String KEY_PATH = "KEY_PATH";

    private Bitmap bitmap;
    private Thread loadImageThread;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_image_view, container, false);
        String strPath = "";

        Bundle bundle = getArguments();
        if (bundle!=null){
            strPath = bundle.getString(KEY_PATH, "");
        }

        if (strPath==null || strPath.isEmpty()){
            getActivity().finish();
        }

        final File imgFile = new  File(strPath);

        if(imgFile.exists()){
            Runnable loadImageRunnable = new Runnable() {
                @Override
                public void run() {
                    Log.d("devcpp","start -> " + Thread.currentThread().getName());
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inJustDecodeBounds = true;

                    if (Thread.currentThread().isInterrupted()) {
                        Log.d("devcpp","Interrupted -> 1 ->" + Thread.currentThread().getName());

                        return;
                    }

                    BitmapFactory.decodeFile(imgFile.getAbsolutePath(), options);

                    Display display = getActivity().getWindowManager().getDefaultDisplay();
                    Point size = new Point();
                    display.getSize(size);
                    int width = size.x;
                    if (Thread.currentThread().isInterrupted()) {
                        Log.d("devcpp","Interrupted -> 2 ->" + Thread.currentThread().getName());

                        return;
                    }

                    int nScale = options.outWidth / width;
                    if (nScale<1){
                        nScale = 1;
                    }

                    if (Thread.currentThread().isInterrupted()) {
                        Log.d("devcpp","Interrupted -> 3 ->" + Thread.currentThread().getName());

                        return;
                    }

                    options = new BitmapFactory.Options();
                    options.inSampleSize = nScale;

                    bitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath(), options);

                    Activity activity = getActivity();
                    if (Thread.currentThread().isInterrupted()) {
                        Log.d("devcpp","Interrupted -> 4 ->" + Thread.currentThread().getName());

                        return;
                    }
                    if (activity != null) {
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                ImageView imageView = view.findViewById(R.id.ImageViewFragment);
                                ProgressBar progressBar = view.findViewById(R.id.progress);

                                imageView.setImageBitmap(bitmap);
                                imageView.setVisibility(View.VISIBLE);
                                progressBar.setVisibility(View.GONE);
                            }
                        });
                    }
                    Log.d("devcpp","stop -> " + Thread.currentThread().getName());
                }
            };

            loadImageThread = new Thread(loadImageRunnable);
            loadImageThread.start();

            int cores = Runtime.getRuntime().availableProcessors();


//            Runnable runnable = new Runnable() {
//                @Override
//                public void run() {
//                    int countR = 0;
//                    int countB = 0;
//                    int countG = 0;
//
//                    long l = 0;
//                    int d = 0;
//
//
//                    while (++d < 10) {
//                        for (int x=0; x < bitmap.getWidth(); x++) {
//                            for (int y=0; y < bitmap.getHeight(); y++) {
//                                int pixel = bitmap.getPixel(x, y);
//                                if (pixel == Color.RED) {
//                                    countR++;
//                                } else if (pixel == Color.BLUE) {
//                                    countB++;
//                                } else if (pixel == Color.GREEN) {
//                                    countG++;
//                                }
//                                l = x * 100000 + y * x + 10;
//                            }
//                        }
//                    }
//
//                    final int fCountR = countR;
//                    final int fCountB = countB;
//                    final int fCountG = countG;
//
//                    getActivity().runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            Toast.makeText(getContext(), "countR -> " + fCountR + "; countB -> " +
//                                    fCountB + "; countG -> " + fCountG, Toast.LENGTH_SHORT).show();
//                        }
//                    });
//                }
//            };
//
////            runnable.run();
//
//            Thread thread = new Thread(runnable);
//            thread.start();

        } else {
            getActivity().finish();
        }
        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if (!isVisibleToUser && loadImageThread != null && loadImageThread.isAlive()) {
            Log.d("devcpp","interrupt -> " + loadImageThread.getName());
            loadImageThread.interrupt();
        }
    }
}
