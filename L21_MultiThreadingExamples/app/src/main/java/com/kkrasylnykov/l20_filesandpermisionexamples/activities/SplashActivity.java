package com.kkrasylnykov.l20_filesandpermisionexamples.activities;

import android.os.Bundle;
import android.os.Handler;

import com.kkrasylnykov.l20_filesandpermisionexamples.R;

public class SplashActivity extends MainActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(MainActivity.getNewInstance(SplashActivity.this));
            }
        }, 2500);
    }
}
