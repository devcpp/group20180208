package com.kkrasylnykov.l20_filesandpermisionexamples.activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationListener;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.NonNull;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.kkrasylnykov.l20_filesandpermisionexamples.R;
import com.kkrasylnykov.l20_filesandpermisionexamples.adapters.FileAdapter;

import java.io.File;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements FileAdapter.OnClickItemListener{

    private static final int REQUEST_PERMISSION_ON_READ_FILE = 1100;

    final int STATUS_START = 0;
    final int STATUS_PROCENT = 1;
    final int STATUS_END = 2;

    private FileAdapter adapter;
    private Toolbar mToolbar;
    private Handler handler;

    public static Intent getNewInstance(Context context){
        return new Intent(context, MainActivity.class);
    }

    private Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        handler = new Handler(){
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);

                switch (msg.what) {
                    case STATUS_START:
                        button.setText("STATUS_START");
                        break;
                    case STATUS_PROCENT:
                        int procant = (int) msg.obj;
                        button.setText("STATUS_PROCENT -> " + procant);
                        break;
                    case STATUS_END:
                        button.setText("STATUS_END");
                        break;
                }
            }
        };

        button = findViewById(R.id.bankBtn);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        Log.d("devcpp", "start ->");
                        handler.sendEmptyMessage(STATUS_START);
                        double j = 0;
                        int count = 500000;
                        int old = -1;
                        try {
                            Thread.sleep(5000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        Handler handlerMain = new Handler(Looper.getMainLooper());

                        for (int i=0; i<count; i++) {
                            j += Math.pow(i, i/10000);
                            final int procent = (int) (((float)i)/count * 100);
                            if (old<procent) {
                                old = procent;
                                Message message = new Message();
                                message.what = STATUS_PROCENT;
                                message.obj = procent;
                                handler.sendMessage(message);
//                                handler.post(new Runnable() {
//                                    @Override
//                                    public void run() {
//                                        ((TextView)v).setText("procent -> " + procent);
//                                    }
//                                });
                                try {
                                    Thread.sleep(500);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
//                                runOnUiThread();
                            }

                        }
                        try {
                            Thread.sleep(2000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        handler.sendEmptyMessage(STATUS_END);
                        Log.d("devcpp", "end -> " + j);
                    }
                });
                thread.start();
//                final Bank bank = new Bank();
//                final int nCount = 500000;
//
//                Thread thread1 = new Thread(new Runnable() {
//                    @Override
//                    public void run() {
//                        for (int i = 0; i<nCount; i++) {
//                            bank.inc();
//                        }
//                        Log.d("devcpp", "bank1 -> " + bank.getCount());
//                    }
//                });
//
//                Thread thread2 = new Thread(new Runnable() {
//                    @Override
//                    public void run() {
//                        for (int i = 0; i<nCount; i++) {
//                            bank.dec();
//                        }
//                        Log.d("devcpp", "bank2 -> " + bank.getCount());
//                    }
//                });
//
//                Log.d("devcpp", "start -> bank -> " + bank.getCount());
//
//                thread1.start();
//                thread2.start();


            }
        });

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.RecyclerView);
        RecyclerView.LayoutManager  layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new FileAdapter();
        adapter.setOnClickItemListener(this);
        recyclerView.setAdapter(adapter);

        if(ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_PERMISSION_ON_READ_FILE);
        } else {
            loadFileData();
        }

        mToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        Drawable menuIconDrawable = ContextCompat.getDrawable(this, R.drawable.ic_menu_black_24dp);
        menuIconDrawable.setColorFilter(ContextCompat.getColor(this, android.R.color.white), PorterDuff.Mode.SRC_ATOP);

        final DrawerLayout drawerLayout = (DrawerLayout) findViewById(R.id.DrawerLayoutMainActivity);
        final View navigationDrawer = findViewById(R.id.NavigationDrawer);

        mToolbar.setNavigationIcon(menuIconDrawable);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.openDrawer(navigationDrawer);
            }
        });

        Drawable menuRightIconDrawable = ContextCompat.getDrawable(this, R.drawable.ic_more_vert_black_24dp);
        menuRightIconDrawable.setColorFilter(ContextCompat.getColor(this, android.R.color.white), PorterDuff.Mode.SRC_ATOP);
        mToolbar.setOverflowIcon(menuRightIconDrawable);

        getSupportActionBar().setTitle("testCode");
    }

    private void loadFileData(){
        ArrayList<String> arrData = getAllFilesPath(Environment.getExternalStorageDirectory());
        adapter.setData(arrData);
        for (String strPath:arrData){
            Log.d("devcpp","strPath -> " + strPath);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode==REQUEST_PERMISSION_ON_READ_FILE){
            if(ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED){
                loadFileData();
            } else {
                Toast.makeText(MainActivity.this, "Need permission!!!!!", Toast.LENGTH_LONG).show();
                finish();
            }
        }
    }

    private ArrayList<String> getAllFilesPath(File file){
        ArrayList<String> arrResult = new ArrayList<>();
        if (file!=null && file.exists()){
            if (file.isFile()) {
                if (file.getName().endsWith(".png") ||
                        file.getName().endsWith(".jpg") ||
                        file.getName().endsWith(".jpeg")){
                    arrResult.add(file.getAbsolutePath());
                }

            } else if (file.isDirectory()){
                File[] arrFiles = file.listFiles();
                if (arrFiles!=null){
                    for (File curFile:arrFiles){
                        arrResult.addAll(getAllFilesPath(curFile));
                    }
                }

            }
        }
        return arrResult;
    }

    @Override
    public void onClickItem(ArrayList<String> arrData, int position) {
        Intent intent = new Intent(this, ViewImageActivity.class);
        intent.putExtra(ViewImageActivity.KEY_POSITION, position);
        intent.putExtra(ViewImageActivity.KEY_FILE_PATHS, arrData);
        //intent.putExtra(ViewImageActivity.KEY_FILE_PATH, str);
        startActivity(intent);
    }

    final static int MENU_ITEM_TIMER = 101;
    final static int MENU_ITEM_SETTINGS = 102;
    final static int MENU_ITEM_DELETE = 103;
    final static int MENU_ITEM_HELP = 104;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuItem itemTime = menu.add(0, MENU_ITEM_TIMER,0,"Timer");
        Drawable drawableIconTimer = ContextCompat.getDrawable(this, R.drawable.ic_timer_black_24dp);
        drawableIconTimer.setColorFilter(ContextCompat.getColor(this, android.R.color.white), PorterDuff.Mode.SRC_ATOP);
        itemTime.setIcon(drawableIconTimer);
        itemTime.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);

        MenuItem itemSettings = menu.add(1, MENU_ITEM_SETTINGS,1,"Settings");
        itemSettings.setEnabled(false);
        Drawable drawableSettings = ContextCompat.getDrawable(this, R.drawable.ic_settings_black_24dp);
        drawableSettings.setColorFilter(ContextCompat.getColor(this, android.R.color.white), PorterDuff.Mode.SRC_ATOP);
        itemSettings.setIcon(drawableSettings);
        itemSettings.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

        MenuItem itemDelete = menu.add(2, MENU_ITEM_DELETE,2,"Delete");
        itemDelete.setEnabled(false);
        Drawable drawableDelete = ContextCompat.getDrawable(this, R.drawable.ic_delete_black_24dp);
        drawableDelete.setColorFilter(ContextCompat.getColor(this, android.R.color.white), PorterDuff.Mode.SRC_ATOP);
        itemDelete.setIcon(drawableDelete);
        itemDelete.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

        MenuItem itemHelp = menu.add(3, MENU_ITEM_HELP,3,"Help");;
        itemHelp.setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case MENU_ITEM_TIMER:
                Toast.makeText(this,"MENU_ITEM_TIMER",Toast.LENGTH_LONG).show();
                break;
            case MENU_ITEM_SETTINGS:
                Toast.makeText(this,"MENU_ITEM_SETTINGS",Toast.LENGTH_LONG).show();
                break;
            case MENU_ITEM_DELETE:
                Toast.makeText(this,"MENU_ITEM_DELETE",Toast.LENGTH_LONG).show();
                break;
            case MENU_ITEM_HELP:
                Toast.makeText(this,"MENU_ITEM_HELP",Toast.LENGTH_LONG).show();
                break;
        }
        return true;
    }

    private static class Bank{
        private static int count = 10000;

        public Bank(){
            count = 10000;
        }

        public void inc(){
            synchronized(Bank.class) {
                count++;
            }
        }

        public static synchronized void dec(){
            count--;
        }

        public int getCount(){
            return count;
        }
    }
}
