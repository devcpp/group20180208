package com.kkrasylnykov.l05_appandeventsexample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private TextView info;

//    private View.OnClickListener listener = new View.OnClickListener() {
//        @Override
//        public void onClick(View v) {
//            switch (v.getId()) {
//                case R.id.okButtonMainActivity:
//                    info.setText("->>>>>>> Test set text!!!! <<<<<<<<-");
//                    break;
//                case R.id.clearButtonMainActivity:
//                    info.setText("");
//                    break;
//            }
//        }
//    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        Button okButton = findViewById(R.id.okButtonMainActivity);
//        okButton.setOnClickListener(this);

        Button clearButton = findViewById(R.id.clearButtonMainActivity);
        clearButton.setOnClickListener(this);
//        okButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                info.setText("->>>>>>> Test set text!!!! <<<<<<<<-");
//            }
//        });

        info = findViewById(R.id.infoTextViewMainActivity);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.okButtonMainActivity:
                info.setText("->>>>>>> Test set text!!!! <<<<<<<<-");
                break;
            case R.id.clearButtonMainActivity:
                info.setText("");
                break;
        }
    }
}
