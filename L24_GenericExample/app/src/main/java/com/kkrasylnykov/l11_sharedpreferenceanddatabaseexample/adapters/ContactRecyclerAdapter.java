package com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.R;
import com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.data.Contact;
import com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.data.Entity;
import com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.data.Phone;

import java.text.MessageFormat;
import java.util.ArrayList;

public class ContactRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private int CONTACT_ITEM = 0;
    private int PHONE_ITEM = 2;
    private int HEADER_ITEM = 1;

    private ArrayList<Entity> showData;
    private OnContactClickListener contactClickListener;

    public ContactRecyclerAdapter(ArrayList<Contact> data) {
        this.showData = new ArrayList<>();
        if (this.showData != null) {
            for (Entity entity : data) {
                this.showData.add(0, entity);
            }
        }
    }

    public void setOnContactClickListener(OnContactClickListener contactClickListener){
        this.contactClickListener = contactClickListener;
    }

    @Override
    public int getItemViewType(int position) {
        int type = CONTACT_ITEM;
        if (position == 0) {
            type = HEADER_ITEM;
        } else if (showData.get(getRealPosition(position)) instanceof Phone) {
            type = PHONE_ITEM;
        }
        return type;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder holder = null;

        if (viewType == HEADER_ITEM) {
            holder = new HeaderViewHolder(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_header, parent, false));
        } else if (viewType == CONTACT_ITEM) {
            holder = new ContactViewHolder(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_contact, parent, false));
        } else if (viewType == PHONE_ITEM) {
            holder = new PhoneViewHolder(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_phone, parent, false));
        }

        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) == HEADER_ITEM) {
            HeaderViewHolder headerViewHolder = (HeaderViewHolder) holder;
            headerViewHolder.count.setText(Integer.toString(showData.size()));
        } else if (getItemViewType(position) == CONTACT_ITEM) {
            ContactViewHolder contactViewHolder = (ContactViewHolder) holder;
            Contact contact = (Contact) showData.get(getRealPosition(position));
            contactViewHolder.name.setText(MessageFormat.format("{0} {1}",
                    contact.getName(), contact.getSname()));
            contactViewHolder.itemState.setRotation(contact.isExpand() ? 180 : 0);
        } else if (getItemViewType(position) == PHONE_ITEM) {
            PhoneViewHolder phoneViewHolder = (PhoneViewHolder) holder;
            Phone phone = (Phone) showData.get(getRealPosition(position));
            phoneViewHolder.phone.setText(phone.getPhone());
        }

    }

    @Override
    public int getItemCount() {
        return this.showData.size() + 1;
    }

    public int getRealPosition(int position) {
        int result = position - 1;
        return result;
    }

    public class HeaderViewHolder extends RecyclerView.ViewHolder {
        TextView count;

        public HeaderViewHolder(View itemView) {
            super(itemView);
            count = itemView.findViewById(R.id.countItem);
        }
    }

    public class ContactViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView name;
        ImageView itemState;
        ImageView edit;

        public ContactViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.nameContactItem);
            itemState = itemView.findViewById(R.id.arrowContactItem);
            edit = itemView.findViewById(R.id.editContactItem);
            itemView.setOnClickListener(this);
            edit.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            int realPosition = getRealPosition(position);
            Contact contact = (Contact) showData.get(realPosition);
            if (v.getId() == R.id.editContactItem) {
                if (contactClickListener != null) {
                    contactClickListener.onContactClick(contact.getId());
                }
            } else {
                if (contact.isExpand()) {
                    showData.removeAll(contact.getPhones());
                    notifyItemRangeRemoved(position+1, contact.getPhones().size());
                } else {
                    showData.addAll(realPosition+1, contact.getPhones());
                    notifyItemRangeInserted(position+1, contact.getPhones().size());
                }
                notifyItemChanged(position);
                contact.setExpand(!contact.isExpand());
            }

        }
    }

    public class PhoneViewHolder extends RecyclerView.ViewHolder {

        TextView phone;

        public PhoneViewHolder(View itemView) {
            super(itemView);
            phone = itemView.findViewById(R.id.phoneItem);
        }
    }

    public interface OnContactClickListener {
        void onContactClick(long id);
    }
}