package com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.providers.db;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.ToolsAndConstants.DbConstants;
import com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.data.Contact;
import com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.data.Entity;
import com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.data.Phone;
import com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.db.DataBaseHelper;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

public abstract class BaseDBProvider<T extends Entity> {

    private DataBaseHelper dbHelper;
    private String tableName;

    public BaseDBProvider(Context context, String tableName){
        dbHelper = new DataBaseHelper(context);
        this.tableName = tableName;
    }

    protected SQLiteDatabase getReadableDatabase(){
        return dbHelper.getReadableDatabase();
    }

    protected SQLiteDatabase getWritableDatabase(){
        return dbHelper.getWritableDatabase();
    }

    protected String getTableName() {
        return tableName;
    }

    public ArrayList<T> getAll(){
        ArrayList<T> result = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = db.query(getTableName(), null, null, null,
                null, null, null);

        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do{
                    result.add(getItem(cursor));
                } while(cursor.moveToNext());
            }
            cursor.close();
        }
        db.close();
        return result;
    }

    public long insertItem(T item){
        SQLiteDatabase db = getWritableDatabase();
        long idPhone = db.insert(getTableName(),
                null,
                item.getContentValues());
        db.close();
        return idPhone;
    }

    public void updateItem(T item){
        SQLiteDatabase db = getWritableDatabase();

        String selection = DbConstants.DB_V2.BASE_TABLE.FIELDS.ID + "=?";
        String[] selectionArgs = new String[]{Long.toString(item.getId())};

        db.update(getTableName(), item.getContentValues(), selection, selectionArgs);

        db.close();
    }

    public void removeItemById(T item){
        removeItemById(item.getId());
    }

    public void removeItemById(long id){
        SQLiteDatabase db = getWritableDatabase();

        String selection = DbConstants.DB_V2.BASE_TABLE.FIELDS.ID + "=?";
        String[] selectionArgs = new String[]{Long.toString(id)};
        db.delete(getTableName(), selection, selectionArgs);
        db.close();
    }

    public void removeAll(){
        SQLiteDatabase db = getWritableDatabase();
        db.delete(getTableName(), null, null);
        db.close();
    }

    protected abstract T getItem(Cursor cursor);
}
