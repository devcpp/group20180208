package com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.ToolsAndConstants;

public class DbConstants {
    public static final String DB_NAME = "notepad_db";
    public static final int DB_VERSION = 2;

    public static class DB_V1 {
        public static final String TABLE_NAME = "_UserInfo";
        public static final String FIELD_ID = "_id";
        public static final String FIELD_NAME = "_name";
        public static final String FIELD_SNAME = "_sname";
        public static final String FIELD_PHONE = "_phone";
        public static final String FIELD_ADRESS = "_adress";
    }

    public static class DB_V2 {
        public static class BASE_TABLE {
            public static class FIELDS {
                public static final String ID = "_id";
            }
        }
        public static class TABLE_USER_INFO {
            public static final String TABLE_NAME = "_UserInfoV2";

            public static class FIELDS {
                public static final String ID = BASE_TABLE.FIELDS.ID;
                public static final String SERVER_ID = "_server_id";
                public static final String NAME = "_name";
                public static final String SNAME = "_sname";
                public static final String ADRESS = "_adress";
            }
        }

        public static class TABLE_PHONES {
            public static final String TABLE_NAME = "_PhonesV2";

            public static class FIELDS {
                public static final String ID = BASE_TABLE.FIELDS.ID;
                public static final String USER_ID = "_user_id";
                public static final String PHONE = "_phone";
            }
        }
    }
}
