package com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.data;

import android.content.ContentValues;
import android.database.Cursor;

import com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.ToolsAndConstants.DbConstants;

import org.json.JSONObject;

public class Phone extends Entity {
    private long userId;
    private String phone;

    public Phone(Cursor cursor) {
        super(-1);
        int idPosition = cursor.getColumnIndex(DbConstants.DB_V2.TABLE_PHONES.FIELDS.ID);
        int userIdPosition = cursor.getColumnIndex(DbConstants.DB_V2.TABLE_PHONES.FIELDS.USER_ID);
        int phonePosition = cursor.getColumnIndex(DbConstants.DB_V2.TABLE_PHONES.FIELDS.PHONE);

        setId(cursor.getLong(idPosition));
        userId = cursor.getLong(userIdPosition);
        phone = cursor.getString(phonePosition);
    }

    public Phone(String phone) {
        this(-1, -1, phone);
    }

    public Phone(long userId, String phone) {
        this(-1, userId, phone);
    }

    public Phone(long id, long userId, String phone) {
        super(id);
        this.userId = userId;
        this.phone = phone;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public ContentValues getContentValues() {
        ContentValues contentValues = new ContentValues();
        contentValues.put(DbConstants.DB_V2.TABLE_PHONES.FIELDS.USER_ID, getUserId());
        contentValues.put(DbConstants.DB_V2.TABLE_PHONES.FIELDS.PHONE, getPhone());
        return contentValues;
    }
}
