package com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.engines;

import android.content.Context;

public abstract class BaseEngine {
    private Context context;

    public BaseEngine(Context context) {
        this.context = context;
    }

    public Context getContext() {
        return context;
    }
}
