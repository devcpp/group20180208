package com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.providers.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.R;
import com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.ToolsAndConstants.DbConstants;
import com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.data.Contact;
import com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.data.Phone;
import com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.db.DataBaseHelper;

import java.text.MessageFormat;
import java.util.ArrayList;

public class ContactDBProvider extends BaseDBProvider<Contact> {
    public ContactDBProvider(Context context) {
        super(context, DbConstants.DB_V2.TABLE_USER_INFO.TABLE_NAME);
    }

    public Contact getItemById(long id){
        Contact contact = null;
        SQLiteDatabase db = getReadableDatabase();

        String selection = DbConstants.DB_V2.TABLE_USER_INFO.FIELDS.ID + "=?";
        String[] selectionArgs = new String[]{Long.toString(id)};

        Cursor cursor = db.query(getTableName(), null,
                selection, selectionArgs,
                null, null, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                contact = new Contact(cursor);
            }

            cursor.close();
        }
        db.close();
        return contact;
    }

    public void insetrItems(ArrayList<Contact> contacts){
        SQLiteDatabase db = getWritableDatabase();
        int count = 0;
        for (Contact contact : contacts) {
            if (count == 0) {
                Log.d("devcpp","beginTransaction");
                db.beginTransaction();
            }
            count++;
            db.insert(getTableName(), null, contact.getContentValues());
            if (count == 10000) {
                db.setTransactionSuccessful();
                db.endTransaction();
                Log.d("devcpp","endTransaction");
                count = 0;
            }
        }
        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();
    }

    @Override
    protected Contact getItem(Cursor cursor) {
        return new Contact(cursor);
    }
}
