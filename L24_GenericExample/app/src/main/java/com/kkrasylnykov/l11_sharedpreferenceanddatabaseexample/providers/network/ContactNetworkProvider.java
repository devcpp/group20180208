package com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.providers.network;

import android.util.Log;

import com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.data.Contact;
import com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.engines.ContactEngine;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

public class ContactNetworkProvider {

    private static final String BASE_URL = "http://46.101.148.28";

    private static final String PATH_GET_ALL = "/api/users.json";
    private static final String PATH_INSERT_ITEM = "/api/users.json";

    public void insertItem(Contact contact){
        try {
            String strURL = BASE_URL + PATH_INSERT_ITEM;
            URL url = new URL(strURL);

            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            connection.setReadTimeout(10000);
            connection.setConnectTimeout(10000);

            connection.setRequestMethod("POST");

            connection.setRequestProperty( "Content-Type", "application/json");
            connection.setRequestProperty( "charset", "utf-8");

            String body = contact.getJSONObject().toString();

            byte[] postData = body.getBytes(StandardCharsets.UTF_8);

            connection.getOutputStream().write(postData);

            connection.connect();

            int responseCode = connection.getResponseCode();

            if (responseCode == 200) {
                InputStream is = connection.getInputStream();
                String strResponse = "";
                if (is!=null){
                    BufferedReader reader = null;
                    reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));

                    if (reader!=null){
                        StringBuilder stringBuilder = new StringBuilder();
                        while (true){
                            String tempStr = reader.readLine();
                            if(tempStr != null){
                                stringBuilder.append(tempStr);
//                                strResponse += tempStr; - NOT TRUE!!!!!
                            } else {
                                strResponse = stringBuilder.toString();
                                break;
                            }
                        }
                        reader.close();
                    }
                    is.close();
                }

                Log.d("devcpp", "response -> " + responseCode);
                Log.d("devcpp", "strResponse -> " + strResponse);

                contact.setServerId(new Contact(new JSONObject(strResponse)).getServerId());
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void getAll(ContactEngine.GetContactCallback callback){
        try {
            String strURL = BASE_URL + PATH_GET_ALL;
            URL url = new URL(strURL);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            connection.setReadTimeout(10000);
            connection.setConnectTimeout(10000);

            connection.setRequestMethod("GET");

            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("charset", "utf-8");

            connection.connect();

            int responseCode = connection.getResponseCode();

            if (responseCode == 200) {
                InputStream is = connection.getInputStream();
                String strResponse = "";
                if (is!=null){
                    BufferedReader reader = null;
                    reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));

                    if (reader!=null){
                        StringBuilder stringBuilder = new StringBuilder();
                        while (true){
                            String tempStr = reader.readLine();
                            if(tempStr != null){
                                stringBuilder.append(tempStr);
//                                strResponse += tempStr; - NOT TRUE!!!!!
                            } else {
                                strResponse = stringBuilder.toString();
                                break;
                            }
                        }
                        reader.close();
                    }
                    is.close();
                }

                Log.d("devcpp", "response -> " + responseCode);
                Log.d("devcpp", "strResponse -> " + strResponse);

                JSONArray jsonArray = new JSONArray(strResponse);
                ArrayList<Contact> data = new ArrayList<>();
                for (int i = 0; i < jsonArray.length(); i++) {
                    data.add(new Contact(jsonArray.getJSONObject(i)));
                }

                callback.onResult(data);
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
