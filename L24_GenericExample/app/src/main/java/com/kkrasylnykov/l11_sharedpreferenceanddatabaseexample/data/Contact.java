package com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.data;

import android.content.ContentValues;
import android.database.Cursor;

import com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.ToolsAndConstants.DbConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Contact extends Entity {

    private long serverId;
    private String name;
    private String sname;
    private ArrayList<Phone> phones;
    private String adress;

    private boolean isExpand = false;

    public Contact(JSONObject jsonObject) throws JSONException {
        super(-1);
        serverId = jsonObject.getLong("id");
        name = jsonObject.getString("name");
        sname = jsonObject.getString("sname");
        phones = new ArrayList<>();
        JSONArray phonesJSONArray = jsonObject.optJSONArray("phones");
        for (int i = 0; i < phonesJSONArray.length(); i++) {
            phones.add(new Phone(phonesJSONArray.getString(i)));
        }
        JSONArray addressJSONArray = jsonObject.optJSONArray("address");
        if (addressJSONArray.length() > 0)  {
            adress = addressJSONArray.getString(0);
        }
    }

    public Contact(Cursor cursor) {
        super(-1);
        int idPosition = cursor.getColumnIndex(DbConstants.DB_V2.TABLE_USER_INFO.FIELDS.ID);
        int serverIdPosition = cursor.getColumnIndex(DbConstants.DB_V2.TABLE_USER_INFO.FIELDS.SERVER_ID);
        int namePosition = cursor.getColumnIndex(DbConstants.DB_V2.TABLE_USER_INFO.FIELDS.NAME);
        int snamePosition = cursor.getColumnIndex(DbConstants.DB_V2.TABLE_USER_INFO.FIELDS.SNAME);
        int adressPosition = cursor.getColumnIndex(DbConstants.DB_V2.TABLE_USER_INFO.FIELDS.ADRESS);

        setId(cursor.getLong(idPosition));
        serverId = cursor.getLong(serverIdPosition);
        name = cursor.getString(namePosition);
        sname = cursor.getString(snamePosition);
        adress = cursor.getString(adressPosition);
    }

    public Contact(String name, String sname, ArrayList<Phone> phones, String adress) {
        this(-1, -1, name, sname, phones, adress);
    }

    public Contact(String name, String sname, String adress) {
        this(-1, -1, name, sname, new ArrayList<Phone>(), adress);
    }

    public Contact(long id, long serverId, String name, String sname, ArrayList<Phone> phones, String adress) {
        super(id);
        this.serverId = serverId;
        this.name = name;
        this.sname = sname;
        this.phones = phones;
        this.adress = adress;
    }

    public long getServerId() {
        return serverId;
    }

    public void setServerId(long serverId) {
        this.serverId = serverId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSname() {
        return sname;
    }

    public void setSname(String sname) {
        this.sname = sname;
    }

    public ArrayList<Phone> getPhones() {
        return phones;
    }

    public void setPhones(ArrayList<Phone> phones) {
        this.phones = phones;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public boolean isExpand() {
        return isExpand;
    }

    public void setExpand(boolean expand) {
        isExpand = expand;
    }

    @Override
    public ContentValues getContentValues() {
        ContentValues contentValues = new ContentValues();
        contentValues.put(DbConstants.DB_V2.TABLE_USER_INFO.FIELDS.SERVER_ID, getServerId());
        contentValues.put(DbConstants.DB_V2.TABLE_USER_INFO.FIELDS.NAME, getName());
        contentValues.put(DbConstants.DB_V2.TABLE_USER_INFO.FIELDS.SNAME, getSname());
        contentValues.put(DbConstants.DB_V2.TABLE_USER_INFO.FIELDS.ADRESS, getAdress());
        return contentValues;
    }

    public JSONObject getJSONObject() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("name", name);
        jsonObject.put("sname", name);
        JSONArray phonesJsonArray = new JSONArray();
        if (phones != null) {
            for (Phone phone : phones) {
                phonesJsonArray.put(phone.getPhone());
            }
        }
        jsonObject.put("phones", phonesJsonArray);

        JSONArray addressJsonArray = new JSONArray();
        if (adress!= null && !adress.isEmpty()) {
            addressJsonArray.put(adress);
        }
        jsonObject.put("address", addressJsonArray);

        return jsonObject;
    }
}
