package com.kkrasylnykov.l11_sharedpreferenceanddatabaseexample.data;

import android.content.ContentValues;

public abstract class Entity {

    private long id;

    public Entity(long id) {
        this.id = id;
    }

    public abstract ContentValues getContentValues();

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object obj) {
        boolean result = false;
        if (obj instanceof Entity && this.getClass().equals(obj.getClass())) {
            Entity phoneObj = (Entity) obj;
            result = this.getId() == phoneObj.getId();
        }
        return result;
    }
}
